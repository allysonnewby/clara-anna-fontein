<?php session_start();?>
<?php 

//CHECK .IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
//SET A BASELINE VALUE FOR VALIDATION CHECK
$vvalidate = 0;
$vvalidemail = 0;
$vdate = date('Y-m-d h:i:s');
//COLLECT ALL VALUES FROM THE FORM & VALIDATE OR SANITIZE IF NECESSARY

//REQUIRED
$vname = filter_var(trim($_POST['txtname']), FILTER_SANITIZE_STRING);
if ($vname === '') {

    $vvalidate++;

}

/************************************************************************************/
$vemail = filter_var(trim($_POST['txtemail']), FILTER_SANITIZE_EMAIL);
if ($vemail === '') {

    $vvalidate++;

} else if(filter_var($vemail, FILTER_VALIDATE_EMAIL) === false){
    $vvalidemail++;
    
}   
    
/************************************************************************************/
$vsub = trim($_POST['txtsubject']);
if ($vsub === '') {

    $vvalidate++;

}    
/************************************************************************************/
$vmsg =  trim($_POST['txtmsg']);
if ($vmsg === '') {

    $vvalidate++;

}   
/************************************************************************************/
if($vvalidate > 0){
   $vqstr = "?k1=f";
    $vqstr .= "&k2=" . urlencode($vname);
}else{
    $vqstr = "?k2=" . urlencode($vname);
}

$vqstr .= "&k3=" . urlencode($vemail);
$vqstr .= "&k4=" . urlencode($vsub);
$vqstr .= "&k5=" . urlencode($vmsg);

if($vvalidemail > 0){
   $vqstr .= "&kemail=invalid";
}    
    
if($vvalidate !== 0 || $vvalidemail !== 0){

    header('Location: contact.php' . $vqstr);
    exit();

    }else{

        //Email Confirmation to user
        $vto = 'allyson@admakers.com';

        //Subject
        $vsubject = $vsub;

        //Message
        $vmessage = '
        <html>
            <head>
                <title>Clara Anna Fontein</title>
            </head>
            <body>
                <table style="background-color:#FEFEFE; font-family: Arial, Verdana, Tahoma; font-size: 14px; letter-spacing: 0.03em; word-spacing: 0.2em; line-height: 1.6em;" cellspacing="0" cellpadding="0" width="600">
                    <tr>
                        <td style="padding: 6px;">
                            <p><pre>'. $vmsg .'</pre></p>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 6px;">

                        </td>
                    </tr>
                </table>
            </body>
        </html>';

        //To send HTML mail, you can set the Content-type header.
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        //Additional headers
        $headers .= "From: ".$vname . "<" . $vemail. "> \r\n";

        //Init mail
        $vemailsent = mail($vto, $vsubject, $vmessage, $headers);

        //END OF MAIL FUNCTION

        if($vemailsent) {

                header('Location: contact.php?kemail=sent');
                exit();

            } else {

                header('Location: contact.php?kmail=failed');
                exit();	

            }
    }

}else{
		
	header('Location: contact.php?kval=f');
	exit();
	
	}
?>