<?php 
session_start();
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div style="color: #F00" class="msg quicksand">Value required!</div>';}		
	}
	
//FUNCTIONS
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Register - Townhouses | Clara Anna Fontein</title>    
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">TOWNHOUSES</h1>
        
    </div>
    <div class="clear_float"></div>
    <p class="quicksand ">The townhouses will be launched during April/May 2016, should you wish to receive more information, please complete the following registration form to ensure your personal details are on our database:</p>
</div>
  
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">TOWNHOUSES</h1>
        
    </div>
    
    <div class="clear_float"></div>
  
</div>    
     <p class="quicksand contact-second-p">The townhouses will be launched during April/May 2016, should you wish to receive more information, please complete the following registration form to ensure your personal details are on our database:</p> 
        <?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'sent'){ ?>
            <div class="quicksand"><strong class="quicksand" style="font-size: 100%;">Thank you for registering.</strong></div><br>

        <?php } ?> 
<section class="section-middle-contact section-register">

<form method="post" action="townhouses-process.php" class="register-form">
    <fieldset class="register-fieldset">
        <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
            <div class="msg_box quicksand">Please fill in the required fields!</div><br>
        <?php } ?>
        <?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'invalid'){ ?>
            <div class="msg_box quicksand" style="font-size: 90%;">Email address appears to be invalid!</div><br>
        <?php } ?>
        <?php if(isset($_GET['ktel']) && $_GET['ktel'] === 'invalid'){ ?>
            <div class="msg_box quicksand" style="font-size: 90%;">Landline appears to be invalid!</div><br>
        <?php } ?>
        <?php if(isset($_GET['kcell']) && $_GET['kcell'] === 'invalid'){ ?>
            <div class="msg_box quicksand" style="font-size: 90%;">Cellphone number appears to be invalid!</div><br>
        <?php } ?>

        <label class="required quicksand">Name</label>
        <?php echo valrequired_warning('k2'); ?>
        <input type="text" name="txtname" autofocus value="<?php echo valissue('k2');?>">

        <label class="required quicksand">Surname</label>
        <?php echo valrequired_warning('k3'); ?>
        <input type="text" name="txtsurname" autofocus value="<?php echo valissue('k3');?>">
        
        <label class="required quicksand">Landline</label>
        <?php echo valrequired_warning('k4'); ?>
        <input type="text" name="txtlandline" autofocus value="<?php echo valissue('k4');?>">
        
        <label class="required quicksand">Cellphone</label>
        <?php echo valrequired_warning('k5'); ?>
        <input type="text" name="txtcell" autofocus value="<?php echo valissue('k5');?>">        
        
        <label class="required quicksand">Your Email</label>
        <?php echo valrequired_warning('k6'); ?>

        <input type="text" name="txtemail" value="<?php echo valissue('k6');?>">
        
        <label class="required quicksand">Date of birth</label>
        <?php echo valrequired_warning('k7'); ?>
        <input type="date" name="txtdob" autofocus value="<?php echo valissue('k7');?>">     
        
        
        
        <label class="required quicksand">Interested in</label><br><br>
        <?php echo valrequired_warning('k8'); ?>
        <select name="txtinterest">
            <option value="">-- Select --</option>
            <option value="Simplex" <?php if(isset($_GET['k8']) && $_GET['k8'] == 'Simplex'){echo 'selected'; }?> >Simplex</option>
            <option value="Duplex" <?php if(isset($_GET['k8']) && $_GET['k8'] == 'Duplex'){echo 'selected'; }?>>Duplex</option>
        </select><br><br>
        
        
        
        <label class="required quicksand">Bedrooms</label><br><br>
        <?php echo valrequired_warning('k9'); ?>
        <select name="txtbedrooms">
            <option value="">-- Select --</option>
            <option value="Bachelor" <?php if(isset($_GET['k9']) && $_GET['k9'] == 'Bachelor'){echo 'selected'; }?>>Bachelor</option>
            <option value="2" <?php if(isset($_GET['k9']) && $_GET['k9'] == '2'){echo 'selected'; }?>>2</option>
            <option value="3" <?php if(isset($_GET['k9']) && $_GET['k9'] == '3'){echo 'selected'; }?>>3</option>
        </select><br><br>  
        
        
        
        <label class="required quicksand">Bathrooms</label><br><br>
        <?php echo valrequired_warning('k10'); ?>
        <select name="txtbathrooms">
            <option value="">-- Select --</option>
            <option value="1.5" <?php if(isset($_GET['k10']) && $_GET['k10'] == '1.5'){echo 'selected'; }?>>1.5</option>
            <option value="2" <?php if(isset($_GET['k10']) && $_GET['k10'] == '2'){echo 'selected'; }?>>2</option>
        </select> <br><br>       
        
        <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity;?>">

        <input type="submit" name="btnsubmit" value="SEND">
    </fieldset> 
</form>

    <div class="clear_float"></div>
</section>


</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){

    var container_width = $(".main-container-contact-wide").width(); 
    var container_height = $(".main-container-contact-wide").height(); 
    
    var percentageoff = (3/100 * container_width)*2;
    
    var new_height = container_height - percentageoff;

    $(".contact-heading").css("height", new_height);
    $(".contact-title").css("line-height", new_height + 'px');
});

</script>
</body>
</html>
