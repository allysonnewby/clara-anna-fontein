<ul id="desktop_menu">
  <li><a href="index.php">HOME</a></li>
  <li><a href="site-plan.php">SITE PLAN</a></li>
  <li><a href="pricelist.php">PRICE LIST</a></li>
  <li><a href="on-site-facilities.php">ON-SITE FACILITIES</a></li>
  <li><a href="locality-map.php">LOCALITY MAP</a></li>
  <li><a href="the-village.php">THE VILLAGE</a></li>
  <li><a href="retirement.php">RETIREMENT</a></li>
  <li><a href="news.php">NEWS</a></li>
  <li><a href="farm-history.php">FARM HISTORY</a></li>
  <li><a href="documents.php">DOCUMENTS</a></li>
  <li><a href="contact.php">CONTACT US</a></li>
  <li class="icon menu-item-margin"><a href="register.php"><img src="images/click-to-buy.jpg"></a></li>
  <li class="icon"><a href="http://www.rabie.co.za/" target="_blank"><img src="images/rabie.jpg"></a></li>
  <li class="icon"><a href="https://www.facebook.com/claraannafonteinestate" target="_blank"><img src="images/facebook.jpg"></a></li>
</ul>
