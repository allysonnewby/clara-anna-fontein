<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Retirement | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/retirement-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/retirement-2.jpg" alt="" id="logo"/>
    </figure>
    <div id="slidy-container">
	<p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        
    <figure id="slidy">
        <img src="images/cgi5.jpg" alt=""/>
        <img src="images/cgi1.jpg"  alt=""/>
    </figure>
        <a href="register-retirement.php"><img src="images/register-your-interest.png" id="red-banner" alt="Phase 2 on show now"></a>
	<?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"ON EARTH THERE IS NO HEAVEN, BUT THERE ARE PIECES OF IT."</h1>
    <h4 class="cinzel jules">- JULES RENARD</h4>
</div>

<section class="section-middle">
	<aside class="aside-1 retirement-aside-1">
    	<figure>
        	<img src="images/retirement-grandpa.jpg" class="boxshadow">
        </figure>
    	<article class="top-article">
        	<p class="quicksand">Secure your retirement home today – one crafted to provide you with absolute comfort, safety, and stimulation to keep the blood pumping.</p>
        </article>
    </aside>

	<aside class="aside-2 retirement-aside-2">
        
    <article class="section-m-aside2-article retirement-middle-article">
        
    	<p class="bilbo middle-p village-middle-p">Reward yourself with a carefree, well rounded and well deserved rest at Clara Anna Fontein Lifestyle Estate.</p>
    </article>
    <div class="clear_float"></div>
        
    <figure >
        <img src="images/retirement-cgi.jpg">
    </figure>
    </aside>
    
    <div class="clear_float"></div>
</section>

<section class="section-bottom">
	<aside class="aside-1-bottom retirement-aside-bottom-1">
    	<figure id="bottom-section-main-fig" class="bottom-fig">
        	<img src="images/retirement-trees.jpg">
        </figure>
        
        
        <div class="clear_float"></div>
    </aside>

	<aside class="aside-2-bottom">
 	   <article class="bottom-article">
           <p class="quicksand retirement-p">Clara Anna Fontein Lifestyle Estate located in Durbanville has 760 residences, offering everything needed to guarantee your future happiness and security; it’s also an excellent investment.</p>
            <p class="bilbo coffee village-dog-p">The Estate will be launched at the end of 2016, but to avoid disappointment, register your interest today!  </p>
        </article>
    
    </aside>
	<div class="clear_float"></div>
</section>

<section class="section-bottom village-bottom-section">
    <figure class="facilities">
        <figcaption class="facilities-heading">
            <h1 class="title title-mid">"I LIKE THIS PLACE AND COULD WILLINGLY WASTE MY TIME IN IT"</h1>
            <h4 class="cinzel jules">William Shakespeare</h4>
        </figcaption>
        <img src="images/retirement-footer.jpg">
    </figure>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('RETIREMENT')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('RETIREMENT')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Retirement')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Retirement')").addClass("current-menu-item-a");
});
    

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
