<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Home | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/3pic-1-web.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/3pic-2-web.jpg" alt="" id="logo"/>
    </figure>
    <?php require('slider.php'); ?>
    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"OUR GREATEST NATURAL RESOURCE IS THE MINDS OF OUR CHILDREN."</h1>
    <h4 class="cinzel jules">- Walter Elias Disney</h4>
</div>

<section class="section-middle">
    <div class="clear_float"></div>
</section>

<section class="section-bottom">

	<div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('HOME')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('HOME')").addClass("current-menu-item-a");
});

</script>
</body>
</html>
