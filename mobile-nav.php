<nav id="mobile">
  <img src="images/hamburger.png" id="hamburger" class="closed">
  <div class="clear_float"></div>
</nav>

<nav class="mobile_nav">
    <ul>
      <li><a href="index.php">HOME</a></li>
      <li><a href="site-plan.php">Site Plan</a></li>
      <li><a href="pricelist.php">Price List</a></li>
      <li><a href="on-site-facilities.php">On-site Facilities</a></li>
      <li><a href="locality-map.php">Locality Map</a></li>
      <li><a href="the-village.php">The Village</a></li>
      <li><a href="retirement.php">Retirement</a></li>
      <li><a href="news.php">News</a></li>
      <li><a href="farm-history.php">Farm History</a></li>
      <li><a href="documents.php">Documents</a></li>
      <li><a href="contact.php">Contact Us</a></li>
      <li class="icon menu-item-margin click-to-buy"><a href="register.php"><img src="images/click-to-buy-mob.jpg"></a></li>
      <li class="icon rabie"><a href="http://www.rabie.co.za/" target="_blank"><img src="images/rabie-mob.jpg"></a></li>
      <li class="icon facebook"><a href="https://www.facebook.com/claraannafonteinestate" target="_blank"><img src="images/facebook-mob.jpg"></a>   
      <div class="clear_float"></div>
    </ul>
</nav>

