<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Farm History | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">OM DIT TE VERSTAAN MOET JY DIT SIEN. DIT BEPROEF. DIT GENIET.</h1>
    <h4 class="cinzel jules">- ANONIEM</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/history-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/history-2.jpg" alt="" id="logo"/>
    </figure>
    
    <div id="slidy-container">
        <p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi4.jpg" alt=""/>
            <img src="images/cgi2.jpg"  alt=""/>
        </figure>
        <img src="images/short-red-flash-home.png" id="red-banner" alt="Phase 2 on show now">
        <?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"DAAR IS GEEN HEMEL OP AARDE, MAAR DAAR IS STUKKIES DAARVAN."</h1>
    <h4 class="cinzel jules">- JULES RENARD</h4>
</div>

<section class="section-middle">
	<aside class="aside-1">   	
    	<article class="top-article">
            <p class="bilbo history-heading">What's in the name?</p>
            <p class="quicksand history-par">It is said that … quite some time ago … a gentleman by the name of Hendrik Olivier married the young Beatrix Verwey van Woerden, and they began their life together on a bountiful farm – possibly somewhere in South Africa. </p>     	
        </article>
        <figure>
        	<img src="images/history-elephant.jpg" class="boxshadow">
        </figure>
    </aside>

	<aside class="aside-2"> 
        <figure class="village-landscape">
            <img src="images/history-landscape.jpg">
        </figure>        
        <article class="history-asrticle">
            <p class="quicksand history-par">A few years later, Hendrik tragically passed away and Beatrix became the owner of the farm, and the first female farmer in the Cape. Before Hendrik Olivier’s passing, they were blessed with a beautiful daughter called Anna, born around the same time they started farming in this area! Some say, shortly thereafter, they were blessed with a second daughter, called Clara, translated as Klare in Dutch, meaning clear. And the Fontein? Well, there is in fact a fountain on the land that flows strongly to this day! </p>
        </article>
    </aside>
    
    <div class="clear_float"></div>
</section>

<section class="section-bottom">
	<aside class="aside-1-bottom">
    	<figure id="bottom-section-main-fig" class="bottom-fig">
        	<img src="http://claraannafontein.co.za/claraannafontein-first-draft/images/history-open-field.jpg">
        </figure>     
        <div class="clear_float"></div>
    </aside>

	<aside class="aside-2-bottom">
 	   <article class="bottom-article">
            <p class="bilbo coffee village-dog-p">And there you have it… the birth of today’s upmarket lifestyle estate, Clara Anna Fontein!</p>
        </article>  
    </aside>
	<div class="clear_float"></div>
</section>

<section class="section-bottom village-bottom-section">
    <figure class="facilities">
        <figcaption class="facilities-heading">
            <h1 class="title title-mid">"I LIKE THIS PLACE AND COULD WILLINGLY WASTE MY TIME IN IT"</h1>
            <h4 class="cinzel jules">William Shakespeare</h4>
        </figcaption>
        <img  class="zebra" src="images/history-footer.jpg">
    </figure>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('FARM')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('FARM')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Farm')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Farm')").addClass("current-menu-item-a");
});

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
