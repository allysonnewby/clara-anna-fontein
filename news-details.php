<?php
$vid = $_GET['kid'];
//CREATE SQL STATEMENT
$sql_news = "SELECT * FROM tblnews WHERE nid = $vid ORDER BY ndatetime DESC";

//CONNECT TO MYSQL SERVER
require('cms/inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_news = mysqli_query($vconnection, $sql_news);

//CREATE AN ASSOCIATIVE ARRAY
$rs_news_rows = mysqli_fetch_assoc($rs_news);	

?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>News | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<?php require('mobile-nav.php'); ?>    
<div id="wrapper">
    
<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<section class="section-middle">
    <figure id="news_details_fig-d">
        <img src="uploaded-images-news/<?php if($rs_news_rows['nimage'] == 'na'){echo 'caf-logo.jpg';}else{echo $rs_news_rows['nimage'];}?>">
    </figure>
    <article class="news-article-d">            
        <h1><?php echo $rs_news_rows['ntitle'];?></h1>
        <?php if($rs_news_rows['ndescription'] != 'na'){?><h3><?php echo $rs_news_rows['ndescription'];?></h3><?php } ?>
        <?php if($rs_news_rows['ncontent'] != 'na'){?><div class="quicksand"><?php echo $rs_news_rows['ncontent'];?></div><?php }?>

        <div class="date-added">
            <p class="date-added quicksand">Posted on: <?php echo $rs_news_rows['ndatetime'];?></p>
        </div>          
      </article>
    <div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('NEWS')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('NEWS')").addClass("current-menu-item-a");
});

</script>
</body>
</html>
