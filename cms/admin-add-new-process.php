<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
//SET A BASELINE VALUE FOR VALIDATION CHECK
$vvalidate = 0;

	//COLLECT ALL VALUES FROM THE FORM & VALIDATE OR SANITIZE IF NECESSARY
	
	$vdate = date('Y-m-d h:i:s');
	
	//REQUIRED
	$vname = trim($_POST['txtname']);
	if ($vname == '') {
		
		$vvalidate++;
		
	}
	/***********************************************************************************/	
	$vsurname = trim($_POST['txtsurname']);
	if ($vsurname == '') {
		
		$vvalidate++;
		
	}
	/************************************************************************************/	
	$vemail = strtolower(trim($_POST['txtemail']));
	if ($vemail != '') {	
	$vemail = filter_var($vemail, FILTER_SANITIZE_EMAIL);
	if($vemail == '') {
		
		$vvalidate++;
			
		}
	
	} else {
		
		$vvalidate++;
		
		} 
		
	/**************************************************************************************/	
	$vusername = sha1($vemail);		
	$vpassword = trim($_POST['txtpassword']);	
		
			if ($vpassword != '') {
				
				$vpassword = filter_var($vpassword, FILTER_SANITIZE_STRING);
				
				if ($vpassword != '') {
					
						$vpassword = sha1($vpassword);
						
					} else {
						
							$vvalidate++;
							
							}
			} 	
	
	/**************************************************************************************/	
	$vpasswordconfirm = trim($_POST['txtpasswordconfirm']);	
	
	$vqstr = "?k1=f";
	$vqstr .= "&k2=" . urlencode($vname);
	$vqstr .= "&k3=" . urlencode($vsurname);
	$vqstr .= "&k4=" . urlencode($vemail);
	$vqstr .= "&k5=" . urlencode($vpassword);
	$vqstr .= "&k6=" . urlencode($vpasswordconfirm);
	
	if($vvalidate !== 0){
		
		header('Location: cms-admin-add-new.php' . $vqstr);
		exit();
		
		}else{

			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE SQL STATEMENT
			$sql_admin = sprintf("INSERT INTO tbladmin (cname, csurname, cemail, cusername, cpassword) VALUES (%s, %s, %s, %s, %s)",
				escapestring($vconnection, $vname, 'text'), 
				escapestring($vconnection, $vsurname, 'text'), 
				escapestring($vconnection, $vemail, 'text'),
				escapestring($vconnection, $vusername, 'text'), 
				escapestring($vconnection, $vpassword, 'text')
				);

				$sql_admin_result = mysqli_query($vconnection, $sql_admin);
				
				if($sql_admin_result){
					
					header('Location: admin-display.php?kadd=true');
					exit();
					
					}else{
						
						header('Location: admin-add-new.php?kadd=f');
						exit();
						
				}				
			}
				
}else{
		
	header('Location: signout.php');
	exit();
	
	}
?>