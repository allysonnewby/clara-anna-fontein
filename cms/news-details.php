<?php require("inc-cms-pre-doctype.php"); ?>
<?php
$vid = $_GET['kid'];
//CREATE SQL STATEMENT
$sql_news = "SELECT * FROM tblnews WHERE nid = $vid ORDER BY ndatetime DESC";

//CONNECT TO MYSQL SERVER
require('inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_news = mysqli_query($vconnection, $sql_news);

//CREATE AN ASSOCIATIVE ARRAY
$rs_news_rows = mysqli_fetch_assoc($rs_news);	

?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
</head>

<body>

<div id="main_container">

<div id="branding_bar">
<?php require("inc-cms-branding-bar.php"); ?>
</div>

<div id="body_column_left_container">

    <div id="body_column_left">
        <?php require("inc-cms-accordion_menu.php"); ?>
    </div>
    
</div>

<div id="body_column_right_container">
    
    <div id="body_column_right">
       <article>
          
       
           <figure id="news_details_fig">
              <img src="../uploaded-images-news/<?php if($rs_news_rows['nimage'] == 'na'){echo 'caf-logo.jpg';}else{echo $rs_news_rows['nimage'];}?>">
           </figure>
          <h1><?php echo $rs_news_rows['ntitle'];?></h1>
           <h3><?php echo $rs_news_rows['ndescription'];?></h3>
          <p><?php echo $rs_news_rows['ncontent'];?></p>
          
          <div id="news_details_date">
              	<p>Posted on: <?php echo $rs_news_rows['ndatetime'];?></p>
          </div>
          
      </article>    
    </div>
        
</div>

<div class="clearfloat_both"></div>
                
</div>


</body>
</html>