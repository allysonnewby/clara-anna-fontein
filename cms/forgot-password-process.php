<?php session_start();?>
<?php 

//CHECK .IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
	//SET A BASELINE VALUE FOR VALIDATION CHECK
	$vvalidate = 0;
	$vdate = date('Y-m-d h:i:s');
	//COLLECT ALL VALUES FROM THE FORM & VALIDATE OR SANITIZE IF NECESSARY
	
	/************************************************************************************/
	$vemail = filter_var(trim($_POST['txtemail']), FILTER_SANITIZE_EMAIL);
	if ($vemail === '') {
		
		$vvalidate++;
		
	}
	/************************************************************************************/
	$vqstr = "?k1=f";

	if($vvalidate !== 0){
		
		header('Location: signin-failed.php' . $vqstr);
		exit();
		
		}else{
			  //Email Confirmation to user
			  $vto = $vemail;
			  
			  //Subject
			  $vsubject = 'Clara Anna Fontein - Reset Password';
			  
			  //Message
			  $vmessage = '
			  <html>
				  <head>
					  <title>Clara Anna Fontein</title>
				  </head>
				  <body>
					  <table style="background-color:#FEFEFE; font-family: Arial, Verdana, Tahoma; font-size: 14px; letter-spacing: 0.03em; word-spacing: 0.2em; line-height: 1.6em;" cellspacing="0" cellpadding="0" width="600">

						  <tr>
							  <td style="padding: 6px;">
								  <p>To update your password please follow this link:</p> 
								  <p><a href="http://claraannafontein.co.za/claraannafontein-first-draft/cms/update-password.php?kemail='. $vemail . '">Update password</a></p>
							
								  <p>Yours faithfully</p>
								  <p>Clara Anna Fontein</p>
							  </td>
						  </tr>
					  </table>
				  </body>
			  </html>';
			  
			  //To send HTML mail, you can set the Content-type header.
			  $headers = "MIME-Version: 1.0\r\n";
			  $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
			  
			  //Additional headers
			   $headers .= "From: Clara Anna Fontein<mail@claraannafontein.co.za>\r\n";
			  
			  //Init mail
			  $vemailsent = mail($vto, $vsubject, $vmessage, $headers);
			  
			  //END OF MAIL FUNCTION
			  if($vemailsent) {
				  
					  header('Location: signin.php?kemail=sent');
					  exit();
					  
				  } else {
					  
					  header('Location: signin.php?kemail=failed');
					  exit();	
				  }
		}
				
}else{
		
	header('Location: signout.php');
	exit();
	
	}
?>