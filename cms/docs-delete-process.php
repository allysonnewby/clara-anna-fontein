<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
		$vid = $_POST['txtid'];
		$pdf = $_POST['txtpdf'];	
			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE SQL STATEMENT
			$sql_delete = sprintf("DELETE FROM tbldocs WHERE pid = %u",
			escapestring($vconnection, $vid, 'int')
			);
				
				$delete_result = mysqli_query($vconnection, $sql_delete);
			
				if($delete_result){
                    
					header('Location: docs-display.php');
					exit();
					
					}else{
						
						header('Location: docs-display.php?kid=' . $vid . '&kdelete=false');
						exit();
						
						}			
					}	

	else{
		
		header('Location: signout.php');
		exit();
	
	}
?>