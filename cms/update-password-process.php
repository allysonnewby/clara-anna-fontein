<?php session_start();?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {

	//SET A BASELINE VALUE FOR VALIDATION CHECK
	$vvalidate = 0;
	$vemail = $_POST['txtemail']; 
	/**************************************************************************************/	
	$vpassword = trim($_POST['txtpassword']);	
	$vpasswordconfirm = trim($_POST['txtpasswordconfirm']);	
			if ($vpassword != '') {
				$vpassword = filter_var($vpassword, FILTER_SANITIZE_STRING);
				if ($vpassword != '') {
						$vpassword = sha1($vpassword);
					} else {
							$vvalidate++;
							}
			} else{
				$vvalidate++;
				}
	
	
	
	/**************************************************************************************/	
	$vqstr = "?k1=f";
	$vqstr .= "&k2=" . urlencode($vemail);

	
	if($vvalidate != 0){
		
		header('Location: update-password.php' . $vqstr);
		exit();
				
		} else{
			
			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE SQL STATEMENT
			$sql_update = sprintf("UPDATE tbladmin SET cpassword = %s WHERE cemail = %s",
				escapestring($vconnection, $vpassword, 'text'),
				escapestring($vconnection, $vemail, 'text') 
				);
				
				$update_result = mysqli_query($vconnection, $sql_update);
			
				if($update_result){
					
					header('Location: signin.php?kpassword=reset');
					exit();
					
					}else{
						
						header('Location: admin-edit.php'. $vqstr . '&kupdate=f');
						exit();
						
						}			
					}	

	}else{
		
		header('Location: signout.php');
		exit();
	
	}
?>