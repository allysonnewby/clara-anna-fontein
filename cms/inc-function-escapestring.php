<?php
if (!function_exists('escapestring')) {
	
	function escapestring($vconntekiah, $value, $datatype) { 
	
	  $value = function_exists('mysqli_real_escape_string') ? mysqli_real_escape_string($vconntekiah, $value) : mysqli_escape_string($vconntekiah, $value);
	
	  switch ($datatype) { 
		case 'text':
		  $value = ($value != '') ? "'" . $value . "'" : "'na'";
		  break;      
		case 'int':
		  $value = ($value != '') ? intval($value) : "'na'";
		  break;  
		case 'float':
		  $value = ($value != '') ? floatval($value) : "'na'";
		  break;
		case 'date':
		  $value = ($value != '') ? "'" . $value . "'" : "'na'";
		  break; 
	  	}
	  	return $value;
		}
	}
?>