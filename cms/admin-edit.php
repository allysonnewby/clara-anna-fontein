<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div class="msg">Value required!</div>';}		
	}
	
//FUNCTION@
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){
		return  $_GET[$vvar];
	}
}	
?>
<?php
//GET VALUE OF kid FROM GET ARRAY
$vaid = $_REQUEST['txtid'];

//CREATE SQL STATEMENT
$sql_admin = "SELECT * FROM tbladmin WHERE cid = $vaid";

//CONNECT TO MYSQL SERVER
require('inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_admin = mysqli_query($vconnection, $sql_admin);

//CREATE AN ASSOCIATIVE ARRAY
$rs_admin_rows = mysqli_fetch_assoc($rs_admin);	

?>

<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
</head>

<body>

<div id="main_container">

    <div id="branding_bar">
    <?php require("inc-cms-branding-bar.php"); ?>
    </div>
    
    <div id="body_column_left_container">
    
        <div id="body_column_left">
            <?php require("inc-cms-accordion_menu.php");?>
        </div>
        
    </div>
    
    <div id="body_column_right_container">
        
        <div id="body_column_right">
            <h2>Edit Admin Details</h2>
            <p><?php ?></p>
			<?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                <div class="msg_box">Please fill in the required fields!</div>
            <?php } ?>
              
            <!--REQUIRED FIELDS -->
            <form method="post" action="admin-edit-process.php" class="frm" onSubmit="return checkp()">
                    <fieldset>
                        <legend>Admin Details</legend>
                            <label class="required">Name</label>
                            <?php echo valrequired_warning('k2'); ?>
                            <input type="text" name="txtname" autofocus value="<?php if(isset($_GET['k2']) && $_GET['k2'] == ''){echo valissue('k2');}else{echo $rs_admin_rows['cname'];}  ?>" required>
                            
                            <label class="required">Surname</label>
                            <?php echo valrequired_warning('k3'); ?>
                            <input type="text" name="txtsurname" value="<?php if(isset($_GET['k3']) && $_GET['k3'] == ''){echo valissue('k3');} else{echo $rs_admin_rows['csurname'];} ?>" required>
                                        
                            <label class="required">Email (Username)</label>
                            <?php echo valrequired_warning('k4'); ?>
                            <input type="text" name="txtemail" value="<?php if(isset($_GET['k4']) && $_GET['k4'] == ''){echo valissue('k4');} else{echo $rs_admin_rows['cemail'];} ?>" required>
                           
                           <p id="msg"></p>
                            <?php if($_SESSION['svadminid'] != $rs_admin_rows['cid']){?>
                           <label>Password</label>
                           <input type="password" name="txtpassword">
                           <input type="hidden" name="txtpasswordencrypt" value="<?php echo $rs_admin_rows['cpassword'];?>">
                           
                           <label>Confirm Password</label>
                           <input type="password" name="txtpasswordconfirm">
                           <?php }?>
                           <input type="hidden" name="txtid" value="<?php echo $rs_admin_rows['cid'];?>">
                           <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
         		           
                           <input type="submit" name="btnupdate" value="Update">
                    </fieldset>
            </form>  
                
        </div>
            
    </div>
    
    <div class="clearfloat_both"></div>
                
</div>


</body>
<script>
<!--    
function checkp(){
	
var p1 = document.forms[0].elements[4].value;
var p2 = document.forms[0].elements[4].value;

if(p1 !== p2){
	
	document.getElementById("msg").innerHTML = "Passwords do not match. Please re-enter";
	document.forms[0].elements[4].value = "";
	document.forms[0].elements[4].focus();
	return false;
	
	}else{
		
		return true;
		
		}
	
}


-->
</script>
</html>