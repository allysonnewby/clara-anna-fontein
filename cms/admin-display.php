<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php
//CREATE SQL STATEMENT
$sql_admin = "SELECT * FROM tbladmin WHERE cstatus NOT LIKE 'd' ORDER BY csurname ASC ";

//CONNECT TO MYSQL SERVER
require('inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_admin = mysqli_query($vconnection, $sql_admin);

//CREATE AN ASSOCIATIVE ARRAY
$rs_admin_rows = mysqli_fetch_assoc($rs_admin);	

?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
<?php 
function obfuscate($vemail){
	return str_replace('@', '&#64', $vemail);
	}

?>
</head>

<body>

<div id="main_container">

<div id="branding_bar">
<?php require("inc-cms-branding-bar.php"); ?>
</div>

<div id="body_column_left_container">

    <div id="body_column_left">

        <?php require("inc-cms-accordion_menu.php");?>
        
       
    </div>
    
</div>

<div id="body_column_right_container">
    
    <div id="body_column_right">
      <h2>Administrators</h2>
        <div>
        <p class="admin_heading"><strong>Name & Surname</strong></p>
        <p class="admin_heading"><strong>Email</strong></p>
        <p class="admin_heading_small"><strong>Status</strong></p>
        <div class="clear_float"></div>
        <?php do{?>
			
		<p class="admin_details"><?php echo $rs_admin_rows['cname'] .' ' . $rs_admin_rows['csurname'];?></p>
        <p class="admin_details"><?php echo $rs_admin_rows['cemail']; ?></p>
        <p class="admin_details_small"><?php if($rs_admin_rows['cstatus'] == 'a'){echo 'Active';}else{echo 'Inactive';} ?></p>
        
        <div class="clear_float"></div>                    	
                <?php if($_SESSION['svadminid'] != $rs_admin_rows['cid']){?>
              	<form method="post" action="admin-status-update-process.php" class="admin_buttons">
                  	<input type="hidden" name="txtid" value="<?php echo $rs_admin_rows['cid'];?>">
                    <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">  
                     
                    <input type="hidden" name="txtstatus" value="<?php echo $rs_admin_rows['cstatus']; ?>">  
                  	<input type="submit" value="<?php if($rs_admin_rows['cstatus'] == 'a'){echo 'Deactivate';}else{ echo 'Activate';} ?>">
                </form>
               <?php }?>
               
                <form method="post" action="admin-edit.php" class="admin_buttons">
                    <input type="hidden" name="txtid" value="<?php echo $rs_admin_rows['cid'];?>">
                    <input type="submit" value="Edit">
                </form>
                
                <?php if($_SESSION['svadminid'] != $rs_admin_rows['cid']){?>
              	<form method="post" action="admin-delete-process.php" onsubmit="return choose()" class="admin_buttons">
                  	<input type="hidden" name="txtid" value="<?php echo $rs_admin_rows['cid'];?>">
                    <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                  	<input type="submit" value="Delete">
                </form> 
                <?php }?>
              	<div class="clear_float"></div>
			
            
            
		<?php } while($rs_admin_rows = mysqli_fetch_assoc($rs_admin))?>


        </div>
    </div>
        
</div>

<div class="clearfloat_both"></div>
                
</div>
<script>
function choose(){
	return confirm("Are you sure you want to delete this?");
	}

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</body>
</html>