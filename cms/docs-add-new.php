<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div class="msg">Value required!</div>';}		
	}
	
//FUNCTIONS
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>

</head>

<body>

<div id="main_container">

    <div id="branding_bar">
    <?php require("inc-cms-branding-bar.php"); ?>
    </div>
    
    <div id="body_column_left_container">
    
        <div id="body_column_left">
            <?php require("inc-cms-accordion_menu.php"); ?>
        </div>
        
    </div>
    
    <div id="body_column_right_container">
        
        <div id="body_column_right">
            <h2>Rentals</h2>
               
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                <div class="msg_box">Please fill in the required fields</div>
            <?php } ?>   
             
             
            
            <form method="post" action="docs-add-new-siteplan-process.php" class="frm" enctype="multipart/form-data">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>Upload new Siteplan</legend>                    
						
						<?php if(isset($_GET['key']) && $_GET['k1'] === 'pdff'){ ?>
                            <div class="msg_box">Please only upload a PDF</div>
                        <?php } ?>        
                    
                        <label class="required">Select Siteplan</label>
                        <input type="file" name="txtsiteplan" >
                        
                        <input type="submit" name="btnsubmit" value="Add"> 
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                
            </form> 
            
            <form method="post" action="docs-add-new-pricelist-process.php" class="frm" enctype="multipart/form-data">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>Upload new Pricelist</legend>                    
						
						<?php if(isset($_GET['kpdfpl']) && $_GET['kpdfpl'] === 'f'){ ?>
                            <div class="msg_box">Please only upload a PDF</div>
                        <?php } ?>        
                    
                        <label class="required">Select Pricelist</label>
                        <input type="file" name="txtpricelist" >
                        
                        <input type="submit" name="btnsubmit" value="Add"> 
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                
            </form>
            
            
            <form method="post" action="docs-add-new-process.php" class="frm" enctype="multipart/form-data">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>Upload Additional Document</legend>
                        <label class="required">Title</label>
                        <?php echo valrequired_warning('k2'); ?>
                        <input type="text" name="txttitle" autofocus value="<?php echo valissue('k2');?>" >                    
						
                        <?php if(isset($_GET['k1']) && $_GET['k1'] === 'pdff'){ ?>
                            <div class="msg_box">Please upload a PDF only</div>
                        <?php } ?>
                    
                        <label class="required">Upload a PDF</label>
                        <input type="file" name="txtpdf" >
                        
                        <input type="submit" name="btnsubmit" value="Add"> 
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                
            </form>            
    
        </div>
            
    </div>
    
    <div class="clearfloat_both"></div>
                
</div>


</body>
</html>