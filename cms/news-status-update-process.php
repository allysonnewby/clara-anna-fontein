<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
$vid = $_POST['txtid'];

$vstatus = $_POST['txtstatus'];	

if($vstatus == 'i'){
	$vstatus = 'a';
	}else{
		$vstatus = 'i';
		}

//CONNECT TO THE MYSQL SERVER
require('inc-connection.php');

//CALL IN THE FUNCTION ESCAPE STRING()
require('inc-function-escapestring.php');

//FORMULATE SQL STATEMENT
$sql_status = sprintf("UPDATE tblnews SET nstatus = %s WHERE nid = %u",
			escapestring($vconnection, $vstatus, 'text'), 
			escapestring($vconnection, $vid, 'int')
			);
	
$status_result = mysqli_query($vconnection, $sql_status);

if($status_result){
	
	header('Location: news-display.php');
	exit();
	
	}else{
		
		header('Location: news-display.php?kid=' . $vid . '&kupdate=false');
		exit();
		
		}			
	}	

else{
	
	header('Location: cms-signout.php');
	exit();

}
?>