<?php require("inc-cms-pre-doctype.php"); ?>
<?php
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {

	//SET A BASELINE VALUE FOR VALIDATION CHECK
	$vvalidate = 0;
	$vdatetime = date('Y-m-d h:i:s');
	$title = filter_var(trim($_POST['txttitle']), FILTER_SANITIZE_STRING);
	if ($title === '') {
		
		$vvalidate++;
		
	}
	
	$vqstr = "?k1=f";
	$vqstr .= "&k2=" . urlencode($title);

    
    $vqstr2 = "?k1=pdff";
	$vqstr2 .= "&k2=" . urlencode($title);

	/************************************************************************************/

if (is_uploaded_file($_FILES['txtpdf']['tmp_name'])) {

    if ($_FILES['txtpdf']['type'] != "application/pdf") {

        header('Location: docs-add-new.php' . $vqstr2);
        exit;

        } else {
            $name = str_replace(' ', '-', strtolower($title)) . '.pdf';
            $result = move_uploaded_file($_FILES['txtpdf']['tmp_name'], "../uploaded-pdfs/". $name);

            if ($result == 0) {
                header('Location: docs-add-new.php?kerror=true');
                exit;
            }

        }

    }else{
    $vvalidate++;
}

				if($vvalidate !== 0){
					header('Location: docs-add-new.php' . $vqstr);
					exit();
		
				}else{
		
					//CONNECT TO THE MYSQL SERVER
					require('inc-connection.php');
					
					//CALL IN THE FUNCTION ESCAPE STRING()
					require('inc-function-escapestring.php');
					
					//FORMULATE SQL STATEMENT
					$sql_insert = sprintf("INSERT INTO tbldocs (pdatetime, ptitle, ppdf) VALUES (%s, %s, %s)",
						escapestring($vconnection, $vdatetime, 'text'), 
						escapestring($vconnection, $title, 'text'),
						escapestring($vconnection, $name, 'text')
						);
						
						$sql_insert_result = mysqli_query($vconnection, $sql_insert);
										
						if($sql_insert_result){
							
							header('Location: docs-display.php');
							exit();
							
							}else{
								
								header('Location: docs-add-new.php?kadd=false');
								exit();
								
						}				
					}
			
}else{
		
	header('Location: signout.php');
	exit();
	
	}
?>