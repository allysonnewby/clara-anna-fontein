<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
		$vid = $_POST['txtid'];
		
		$vstatus = $_POST['txtstatus'];	
		
		if($vstatus == 'i'){
			$vstatus = 'a';
			}else{
				$vstatus = 'i';
				}
		
			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE SQL STATEMENT
			$sql_delete = sprintf("UPDATE tbladmin SET cstatus = %s WHERE cid = %u",
						escapestring($vconnection, $vstatus, 'text'), 
						escapestring($vconnection, $vid, 'int')
						);
				
				$delete_result = mysqli_query($vconnection, $sql_delete);
			
				if($delete_result){
					
					header('Location: admin-display.php');
					exit();
					
					}else{
						
						header('Location: admin-display.php?kid=' . $vid . '&kupdate=false');
						exit();
						
						}			
					}	

	else{
		
		header('Location: cms-signout.php');
		exit();
	
	}
?>