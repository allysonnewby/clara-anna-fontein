<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php
$per_page = 8;

if(isset($_GET['page'])){
	$page = $_GET['page'];
	}else{
		$page = 1;
		}

//pAGE WILL START FROM  0 and multiply by per page
$start_from = ($page-1) * $per_page;

//CREATE SQL STATEMENT
$sql_news = "SELECT * FROM tblnews WHERE nstatus NOT LIKE 'd' ORDER BY nstatus ASC, ndatetime ASC LIMIT $start_from, $per_page";

//CONNECT TO MYSQL SERVER
require('inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_news = mysqli_query($vconnection, $sql_news);

//CREATE AN ASSOCIATIVE ARRAY
$rs_news_rows = mysqli_fetch_assoc($rs_news);

$rs_news_total_rows = mysqli_num_rows($rs_news);
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
</head>

<body>

<div id="main_container">

<div id="branding_bar">
<?php require("inc-cms-branding-bar.php"); ?>
</div>

<div id="body_column_left_container">
    <div id="body_column_left">
        <?php require("inc-cms-accordion_menu.php"); ?>
    </div>
</div>

<div id="body_column_right_container">
    
    <div id="body_column_right">

          <?php do{?>  
            <?php if($rs_news_total_rows !== 0){?>
            <article id="news">
                <a href="news-details.php?kid=<?php echo $rs_news_rows['nid']; ?>"><h2><?php echo $rs_news_rows['ntitle']; ?></h2></a>             
                <p>Date Added: <?php echo $rs_news_rows['ndatetime']; ?></p>
                <p>Status: <?php if($rs_news_rows['nstatus'] == 'a'){echo 'Active';}else{echo 'Inactive';}?></p>
                <div class="clear_float"></div>
            </article>
            
           <div id="buttons">
                <form method="post" action="news-status-update-process.php"> 
                	<input type="hidden" name="txtstatus" value="<?php echo $rs_news_rows['nstatus']; ?>"> 
                	<input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid']; ?>"> 
                	<input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>"> 
                	<input type="submit" class="btnstatus" value="<?php if($rs_news_rows['nstatus'] == 'a'){echo 'Deactivate';}else{ echo 'Activate';} ?>">
                 </form>
              
                 <form method="post" action="news-edit.php" class="events_button">
                    <input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid'];?>">
                    <input type="submit" value="Edit">
                 </form>
                 
                <form method="post" action="news-delete-process.php" onsubmit="return choose()" class="events_button">
                    <input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid'];?>">
                    <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>"> 
                    <input type="submit" value="Delete">
                </form>
                
                <div class="clear_float"></div>
              
            </div>
            <?php } else{
				echo "<br> No houses currently to rent";
				} ?>
		<?php } while($rs_news_rows = mysqli_fetch_assoc($rs_news))?>


        
        <div id="page_num_display">
			<?php
            $query = "SELECT * FROM tblnews";
			
			$result = mysqli_query($vconnection, $query);
			
			//COUNT TOTAL NUMBER OF RECORDS
			$total_records = mysqli_num_rows($result);
			
			//USING CEIL FUNCTION TO DIVIDE TOTAL NUMBER OF RECORS ON PAGE AND ROUND UP TO NEAREST WHOLE NUMBER
			$total_pages = ceil($total_records / $per_page);
			
            //GPING TO FIRST PAGE
			echo "<center><a href=rentals-display.php?page=1'>First Page</a>";
			
			for($i=1; $i<=$total_pages; $i++){
				
				echo "<a href='rentals-display.php?page=".$i."'>".$i."</a>";
				
				}
				
				echo "<a href='rentals-display.php?page=$total_pages'>Last Page</a>";
		
            ?>
        </div>
    </div>
        
</div>

<div class="clearfloat_both"></div>
                
</div>
<script>
function choose(){
	return confirm("Are you sure you want to delete this?");
	}

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</body>
</html>