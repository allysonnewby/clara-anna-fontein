<?php session_start() ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- This piece of valid code tells mobile devices not to zoom out as a default. -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    
	<title>Sign In | Clara Anna Fontein</title>

	<script type="text/javascript" src="../imgsizer.js"></script>
	<script type="text/javascript">
		addLoadEvent(function() {
		if (document.getElementById && document.getElementsByTagName) {
			var aImgs = document.getElementById("content").getElementsByTagName("img");
			imgSizer.collate(aImgs);
			}
		});
	</script>
    <link href="../stylesheets/html_css_reset.css" rel="stylesheet" type="text/css">
	<link href="../stylesheets/public.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/cssmain.css" rel="stylesheet" type="text/css">
    <?php require('inc-cms-head-content.php'); ?>
</head>
<body>
	<!-- WRAPPER -->
    <div id="wrapper">
    
		<!-- CONTENT CONTAINER -->
		<div id="content_container">
            <img src="../images/caf-logo.jpg" id="logo">
            <h1>Sign In</h1>
            
            <!--NOT VALID EMAIL-->
			<?php if ((isset($_POST['kvalidate']) && $_POST['kvalidate'] === 'failed') || 
            (isset($_POST['ksignin']) && $_POST['ksignin'] === 'failed')){ ?>
                <div class="error">Username or password incorrect. <a href="../signin-failed.php">Lost your password?</a></div>
            <?php } ?>
            <!--AFTER PASSSORD FORGET MAIL SENT SUCESSFULLY-->
			<?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'sent'){ ?>
                <div class="error">An Email has been sent with instructuctions to reset your password.</div>
            <?php } ?>
            <!--AFTER PASSSORD FORGET MAIL NOT SENT-->
			<?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'failed'){ ?>
                <div class="error">Signin failed, please try again</div>
            <?php } ?>
            
            <?php if(isset($_GET['kpassword']) && $_GET['kpassword'] === 'reset'){ ?>
                <div class="error">Password has been reset. Please sign in with your new password.</div>
                <br>
       		<?php } ?>
                        
            <form id="signin" method="post" action="signin-process.php" >
                <br>
                <input type="text" name="txtusername" autocomplete="on" required autofocus placeholder="Email">
                <br><br>      
                <input type="password" name="txtpassword" required placeholder="Password">
                <br><br>
                <!--HIDDEN FIELD SESSION VARIABLE KEY VALUE PAIR-->
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>"> 
                  
                <input type="submit" name="btnsignin" value="Sign In" >
            </form>
            
            <br>
            
            <p id="p1"><a id="resetlink" href="forgot-password.php">Forgot Your Password?</a></p>
         </div>

    </div>
</body>
</html>


