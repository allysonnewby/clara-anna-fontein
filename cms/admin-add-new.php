<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div class="msg">Value required!</div>';}		
	}
	
//FUNCTION@
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){echo  $_GET[$vvar];
	}
}	
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
</head>

<body>

<div id="main_container">

    <div id="branding_bar">
    <?php require("inc-cms-branding-bar.php"); ?>
    </div>
    
    <div id="body_column_left_container">
    
        <div id="body_column_left">
            <?php require("inc-cms-accordion_menu.php");?>
        </div>
        
    </div>
    
    <div id="body_column_right_container">
        
        <div id="body_column_right">
            <h2>Add new admin member</h2>
               
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                <div class="msg_box">Please fill in the required fields!</div>
            <?php } ?>   
                
            <form method="post" action="admin-add-new-process.php" class="frm" onSubmit="return checkp()">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>General Administrator information</legend>
                        <label class="required">Name</label>
                        <?php echo valrequired_warning('k2'); ?>
                        <input type="text" name="txtname" autofocus value="<?php echo valissue('k2');?>" required>
                        
                        <label class="required">Surname</label>
                        <?php echo valrequired_warning('k3'); ?>
                        <input type="text" name="txtsurname" value="<?php echo valissue('k3');?>" class="required">
                                    
                        <label class="required">Email (Username)</label>
                        <?php echo valrequired_warning('k4'); ?>
                        <input type="text" name="txtemail" value="<?php echo valissue('k4');?>" class="required">
                        
   				        <p id="msg"></p>
                        <label class="required">Password</label>
                        <?php echo valrequired_warning('k5'); ?>
                        <input type="password" name="txtpassword">
						
                        
                        <label class="required">Confirm Password</label>
                        <?php echo valrequired_warning('k6'); ?>
                        <input type="password" name="txtpasswordconfirm">
                        
                        
                        <input type="submit" name="btnsubmit" value="Add">
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                
            </form>            
    
        </div>
            
    </div>
    
    <div class="clearfloat_both"></div>
                
</div>

</body>
<script>
<!--    
function checkp(){
	
var p1 = document.forms[0].elements[4].value;
var p2 = document.forms[0].elements[5].value;

if(p1 !== p2){
	
	document.getElementById("msg").innerHTML = "Passwords do not match. Please re-enter";
	document.forms[0].elements[5].value = "";
	document.forms[0].elements[5].focus();
	return false;
	
	}else{
		
		return true;
		
		}
	
}
-->
</script>
</html>