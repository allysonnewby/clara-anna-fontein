<div class="applemenu">

<!-- ADMIN STAFF STARTS -->
<div class="silverheader">
    <a href="#">Administrators</a>
</div>
<div class="submenu">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td align="left" class="accmenu"><a href="admin-display.php">Display</a></td>
        </tr>

        <tr>
            <td align="left" class="accmenu"><a href="admin-add-new.php">Add new administrator</a></td>
        </tr>
    </table>   
</div>
<!-- ADMIN STAFF ENDS -->


<!-- NEWS STARTS -->
<div class="silverheader">
    <a href="#">News</a>
</div>
<div class="submenu">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td align="left" class="accmenu"><a href="news-display.php">Display</a></td>
        </tr>

        <tr>
            <td align="left" class="accmenu"><a href="news-add-new.php">Add new article</a></td>
        </tr>
    </table>   
</div>
<!-- NEWS ENDS -->
    
<!-- DOCUMENTS STARTS -->
<div class="silverheader">
    <a href="#">Documents</a>
</div>
<div class="submenu">
    <table width="100%" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td align="left" class="accmenu"><a href="docs-display.php">Display</a></td>
        </tr>

        <tr>
            <td align="left" class="accmenu"><a href="docs-add-new.php">Upload new PDF</a></td>
        </tr>
    </table>   
</div>
<!-- DOCUMENTS ENDS -->    
</div>