<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php

//PAGINATION!!!!!/

$per_page = 4;

if(isset($_GET['page'])){
	
	$page = $_GET['page'];
	
	}else{
		
		$page = 1;
		
		}

//pAGE WILL START FROM  0 and multiply by per page
$start_from = ($page-1) * $per_page;

//CREATE SQL STATEMENT
$sql_news = "SELECT * FROM tblnews ORDER BY ndatetime DESC LIMIT $start_from, $per_page";

//CONNECT TO MYSQL SERVER
require('inc-conntekiah.php');

//EXECUTE SQL STATEMENT
$rs_news = mysqli_query($vconntekiah, $sql_news);

//CREATE AN ASSOCIATIVE ARRAY
$rs_news_rows = mysqli_fetch_assoc($rs_news);	
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>

<script>
function changestatusnews(vid) {
	var xmlhttp = new XMLHttpRequest();
      
    if(window.XMLHttpRequest) {
        //code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        
        } else {  //code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
    
    xmlhttp.onreadystatechange = function() {
         if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
             if(xmlhttp.responseText == "deactive") {
                 
                 document.getElementById("btnstatus" + vid).value = "Activate";
                 document.getElementById("txtstatus").value = "i";
                 
             } else if(xmlhttp.responseText == "active"){
				 
                 document.getElementById("btnstatus" + vid).value = "Deactivate";
                 document.getElementById("txtstatus").value = "a";
				 }
            }
        }
    
vstatus = document.getElementById('txtstatus').value;

xmlhttp.open("GET", "news-status-update-process.php?txtid="+vid+"&txtstatus="+vstatus,true);

xmlhttp.send();
}
</script>

</head>

<body>

<div id="main_container">

<div id="branding_bar">
<?php require("inc-cms-branding-bar.php"); ?>
</div>

<div id="body_column_left_container">
    <div id="body_column_left">
        <?php require("inc-cms-accordion_menu.php"); ?>
    </div>
</div>

<div id="body_column_right_container">
    
    <div id="body_column_right">
      <h2>News Page</h2>
          <?php do{?>  
            
            <article id="news">
            	<figure id="news_fig">
                	<img src="../uploaded-images/<?php if($rs_news_rows['nimgthumb'] == 'na'){echo 'thumb_sample.jpg';}else{echo $rs_news_rows['nimgthumb'];}?>">
                </figure>
                
                <div id="news_content">
                    <a href="news-details.php?kid=<?php echo $rs_news_rows['nid']; ?>"><h3><?php echo $rs_news_rows['ntitle']; ?></h3></a>
                    <p><?php echo $rs_news_rows['nsummary']; ?></p>
                </div>
            </article>
            
            
            <div id="buttons">
                <form> 
                	<input type="hidden" id="txtstatus" value="<?php echo $rs_news_rows['nstatus']; ?>">  
                	<input type="button" class="btnstatus" id="btnstatus<?php echo $rs_news_rows['nid'];?>" onclick="changestatusnews('<?php echo $rs_news_rows['nid'];?>')" value="<?php if($rs_news_rows['nstatus'] == 'a'){echo 'Deactivate';}else{ echo 'Activate';} ?>">
                 </form>
              
                 <form method="post" action="news-edit.php" class="events_button">
                 
                    <input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid'];?>">
                    <input type="submit" value="Edit">
                 </form>
                 
                <form method="post" action="news-delete-process.php" onsubmit="return choose()" class="events_button">
                    <input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid'];?>">
                    <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                    <input type="submit" value="Delete">
                </form>
                
                <div class="clear_float"></div>
              
            </div>

            
		<?php } while($rs_news_rows = mysqli_fetch_assoc($rs_news))?>


        
        <div id="page_num_display">
			<?php
            $query = "SELECT * FROM tblnews";
			
			$result = mysqli_query($vconntekiah, $query);
			
			//COUNT TOTAL NUMBER OF RECORDS
			$total_records = mysqli_num_rows($result);
			
			//USING CEIL FUNCTION TO DIVIDE TOTAL NUMBER OF RECORS ON PAGE AND ROUND UP TO NEAREST WHOLE NUMBER
			$total_pages = ceil($total_records / $per_page);
			
            //GPING TO FIRST PAGE
			echo "<center><a href=cms-news-display.php?page=1'>First Page</a>";
			
			for($i=1; $i<=$total_pages; $i++){
				
				echo "<a href='cms-news-display.php?page=".$i."'>".$i."</a>";
				
				}
				
				echo "<a href='cms-news-display.php?page=$total_pages'>Last Page</a>";
		
            ?>
        </div>
    </div>
        
</div>

<div class="clearfloat_both"></div>
                
</div>
<script>
function choose(){
	return confirm("Are you sure you want to delete this?");
	}

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</body>
</html>