<!-- META DATA -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Expires" content="0"> 
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
<meta name="description" content ="" />
<!--#################### TEACHES OLDER BROWSERS HTML5 ######################## -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--#################### IE BUG FIX ######################## -->
<!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!-- 
Image resizer script.
The code doesn't work in IE6 or less, so I suggest the following
conditionally-commented code in your <head> be used to implement the
JavaScript on your RWD website
-->
<!--[if ! lte IE 6]><!-->

<!--<![endif]-->
<!-- The following script allows all browsers to support media queries in CSS.-->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueriesjs.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<title>Clara Anna Fontein</title>

<!-- ########  EXTERNAL STYLE SHEET  ########### -->
<link rel="stylesheet" type="text/css" href="stylesheets/cssmain.css">

<!-- JAVASCRIPT EXTERNAL FILES FOR ACCORDION MENU -->
<script language="JavaScript" type="text/javascript" src="javascriptfiles/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="javascriptfiles/ddaccordion.js">
</script>
<script type="text/javascript" src="javascriptfiles/ddaccordion_options.js">
</script>

<link href="stylesheets/css_accordion.css" rel="stylesheet" type="text/css">
<!-- END OF JAVASCRIPT EXTERNAL FILES FOR ACCORDION MENU -->
<!-- CK EDITOR -->
<script src="ckeditor/ckeditor.js"></script>

<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href="../favicon.ico">
