<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//FUNCTION FOR UPLOADING IMAGE TO A SERVER LOCATION FROM TEMP LOCATION & RESIZING
function imageValidation($file_name, $file_ext, $file_size, $file_tmp, $file_error) {
		
	$extensions = array("jpeg", "jpg", "png"); //DEFINE AN ARRAY OF PERMITTED FILE EXTENSIONS
		
	//CHECK IF THERE IS A VALUE AND TEST FOR ANY ERRORS
	if($file_error == 0) {
		
		$file_name = filter_var($file_name, FILTER_SANITIZE_STRING);//STRIPS TAGS
		//IF FILE EXTENSION IS JPEG EXCLUDE LAST 5 CHARACTERS IN STRING REPLACEMENT
		if ($file_ext == 'jpeg') { 
			//REMOVE UNWANTED CHARACTERS AND REAPLACE STRING WITH A UNIQUE ID GENERATED BASE ON CURRENT MICRO TIME 
			$file_name = substr_replace(preg_replace("/[-_ ()]/", " ", $file_name), uniqid(), 0, -5); 
			
			} else {
					$file_name = substr_replace(preg_replace("/[-_ ()]/", " ", $file_name), uniqid(), 0, -4); 
					}
		
		//THEN COMPARE FILE EXTENSION CORRESPOND WITH OUR ARRAY
		if(in_array($file_ext, $extensions) === true) {

			//THEN CHECK THAT FILE SIZE DOES NOT EXCEED 2MB//CHECK IF FORMAT IS VALID AND SIZE DOES NOT EXCEED MAX FILE SIZE
			if($file_size < 2000000) {
				
				return $file_name;
				
				} else {
					header('Location: news-edit.php?key=filesizeexceeded' . $vqstr2);
					exit();
					}

			} else {
				header('Location: news-edit.php?key=filenotsupported' . $vqstr2);
				exit();
				}
				
		} else {
			header('Location: news-edit.php?key=fileerror' . $vqstr2);
			exit();
			}	
}
function imageResize($image, $imgdimension) {
	//GET ORIGINAL DIMESIONS FOR SOURCE IMAGE AND SPECIFY NEW DESIRED DIMENSIONS
	$oldwidth = imagesx($image); 
	$oldheight = imagesy($image);
	$newwidth = $imgdimension;
	$newheight = ($oldheight/$oldwidth) * $newwidth; //CALCULATE RATIO OF OLD IMAGE AND APPLY TO NEW WIDTH TO GET A PROPORTIONAL HEIGHT
	$file_tmp = imagecreatetruecolor($newwidth, $newheight); //CREATE TEMP BLANK IMAGE CANVAS WITH NEW DIMENSIONS
	
	imagecopyresampled($file_tmp, $image, 0, 0, 0, 0, $newwidth, $newheight, $oldwidth, $oldheight);//COPY SOURCE IMAGE INTO AN TMP IMG
	
	return $file_tmp;
	}
?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
//SET A BASELINE VALUE FOR VALIDATION CHECK
$vvalidate = 0;

//COLLECT ALL VALUES FROM THE FORM & VALIDATE OR SANITIZE IF NECESSARY
$vid = $_POST['txtid'];

//REQUIRED
$title = filter_var(trim($_POST['txttitle']), FILTER_SANITIZE_STRING);
if ($title === '') {

    $vvalidate++;

}
/***********************************************************************************/
$description = filter_var(trim($_POST['txtdescription']), FILTER_SANITIZE_STRING);
    $descriplength = strlen($description);
    if($descriplength > 500){

      $vvalidate++;  
    }
    //echo $description; exit;
/************************************************************************************/
$content = trim($_POST['txtcontent']);
if ($content == '') {

    $vvalidate++;

}

/************************************************************************************/
$imgold = $_POST['txtimgold'];
    
$image_file = $_FILES['txtimglarge']['name'];
$file_name = strtolower(pathinfo($image_file, PATHINFO_BASENAME)); //GET THE BASENAME. USE BUILT IN FUNCTION WHERE POSSIBLE
$file_ext = pathinfo($file_name, PATHINFO_EXTENSION); //USE BUILT IN FUNCTION WHERE POSSIBLE 
$file_size = $_FILES['txtimglarge']['size'];
$file_tmp = $_FILES['txtimglarge']['tmp_name']; //TMP NAME ALLOCATED WHILE FILE IS SITTING IN THE BROWSER TEMP MEMORY
$file_error = $_FILES['txtimglarge']['error'];

$vqstr = "?k1=f";
$vqstr .= "&k2=" . urlencode($title);
$vqstr .= "&k3=" . urlencode($description);
$vqstr .= "&k4=" . urlencode($descriplength);
$vqstr .= "&k5=" . urlencode($content);
$vqstr .= "&txtid=" . urlencode($vid);

$vqstr2 = "?k1=imgf";
$vqstr2 .= "&k2=" . urlencode($title);
$vqstr2 .= "&k3=" . urlencode($description);
$vqstr2 .= "&k4=" . urlencode($descriplength);
$vqstr2 .= "&k5=" . urlencode($content);
$vqstr2 .= "&txtid=" . urlencode($vid);    

if($file_name !== ''){

        $imagesize = getimagesize($file_tmp);

        if(!$imagesize){

            header('Location: news-edit.php?key=notanimage'. $vqstr2);
            exit();

            }else{

            $vimgoriginal = imageValidation($file_name, $file_ext, $file_size, $file_tmp, $file_error);

            //DEFINE THE ORIGINAL IMAGE PATH THAT HAS PASSED ALL THE TESTS FROM THE SERVER LOCATION SO WE CAN MANIPULATE IT 
            if($file_ext === 'jpeg' || $file_ext === 'jpg') {

            /*************************************************************/	

            $jpegimagelargefrmsrc = imagecreatefromjpeg($file_tmp); 
            $newimglarge = imageResize($jpegimagelargefrmsrc, 1100); 
            $newimglargebasename = 'large_'.$vimgoriginal; 
            $vimglargefilepath = '../uploaded-images-news/'.$newimglargebasename; 
            imagejpeg($newimglarge, $vimglargefilepath, 100);

            //CLEAN UP TMP FILES AND OBJECTS NO LONGER REQUIRED TO SAVE STORAGE SPACE
            imagedestroy($jpegimagelargefrmsrc);
            imagedestroy($newimglarge);

                }
            }

            $unlinkold = '../uploaded-images-news/'.$vimgold;
           
            unlink($unlinkold);

} else {

        $newimglargebasename = $imgold;
    }
           
            if($vvalidate !== 0){

                header('Location: news-edit.php' . $vqstr);
                exit();

            }else{

                //CONNECT TO THE MYSQL SERVER
                require('inc-connection.php');

                //CALL IN THE FUNCTION ESCAPE STRING()
                require('inc-function-escapestring.php');

                //FORMULATE SQL STATEMENT
                $sql_news = sprintf("UPDATE tblnews SET ntitle = %s, ndescription = %s, ncontent = %s, nimage = %s WHERE nid = $vid",
                    escapestring($vconnection, $title, 'text'), 
                    escapestring($vconnection, $description, 'text'), 
                    escapestring($vconnection, $content, 'text'),
                    escapestring($vconnection, $newimglargebasename, 'text')
                    );

                    $sql_news_result = mysqli_query($vconnection, $sql_news);

                    if($sql_news_result){

                        header('Location: news-display.php');
                        exit();

                        }else{

                            header('Location: news-edit.php?kedit=false');
                            exit();

                    }				
                }





			
}else{
		
	header('Location: signout.php');
	exit();
	
	}
?>