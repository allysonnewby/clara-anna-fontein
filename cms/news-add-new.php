<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div class="msg">Value required!</div>';}		
	}
	
//FUNCTIONS
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
<script src="ckeditor/ckeditor.js"></script>
<script>
function toolbar(x){
	CKEDITOR.replace(x, {
		on: 
		{
			instanceReady : function(ev)
			{
				//Output paragraphs as <p>Text</p>
				this.dataProcessor.writer.setRules('p',
					{
						indent: false,
						breakBeforeOpen: false,
						breakAfterOpen: false,
						breakBeforeClose: false,
						breakAfterClose: false
					});
				}
			}
		});
}
</script>

</head>

<body>

<div id="main_container">

    <div id="branding_bar">
    <?php require("inc-cms-branding-bar.php"); ?>
    </div>
    
    <div id="body_column_left_container">
    
        <div id="body_column_left">
            <?php require("inc-cms-accordion_menu.php"); ?>
        </div>
        
    </div>
    
    <div id="body_column_right_container">
        
        <div id="body_column_right">
            <h2>Rentals</h2>
               
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                <div class="msg_box">Please fill in the required fields</div>
            <?php } ?>   
             
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'imgf'){ ?>
                <div class="msg_box">Image Failed</div>
            <?php } ?> 
            
            <?php if(isset($_GET['k4']) && $_GET['k4'] > 500){ ?>
                <br><div class="msg_box">Description too long</div>
            <?php } ?>
            <form method="post" action="news-add-new-process.php" class="frm" enctype="multipart/form-data">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>Add a new article</legend>
                        <label class="required">Title</label>
                        <?php echo valrequired_warning('k2'); ?>
                        <input type="text" name="txttitle" autofocus value="<?php echo valissue('k2');?>" >
                        
                        <label>Short Description - max 500 characters</label>
                        <input type="text" name="txtdescription" value="<?php echo valissue('k3');?>">                    
                                    
                        <label>Article Content</label>
                        <textarea name="txtcontent"><?php echo valissue('k5');?></textarea>
                        <script>toolbar('txtcontent');</script>
                    
						<br>
						<?php if(isset($_GET['key']) && $_GET['key'] === 'notanimage'){ ?>
                            <div class="msg_box">The file you selected is not an image</div>
                        <?php } ?>
                        
						<?php if(isset($_GET['key']) && $_GET['key'] === 'filenotsupported'){ ?>
                            <div class="msg_box">This file extension is not supported, please only upload a JPEG or JPG</div>
                        <?php } ?> 
                        
                        <?php if(isset($_GET['key']) && $_GET['key'] === 'filesizeexceeded'){ ?>
                            <div class="msg_box">File size too large, please upload an image no larger than 1mb</div>
                        <?php } ?>   
                          
                        <label >Upload an Image - please select only images with 'jpg' or 'jpeg' file extensions</label>
                        <input type="file" name="txtimg" >
                        
                        <input type="submit" name="btnsubmit" value="Add"> 
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                
            </form>            
    
        </div>
            
    </div>
    
    <div class="clearfloat_both"></div>
                
</div>


</body>
</html>