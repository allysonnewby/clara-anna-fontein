<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {

	//SET A BASELINE VALUE FOR VALIDATION CHECK
	$vvalidate = 0;
	$vid = $_POST['txtid'];
	
	$vname = trim($_POST['txtname']);
	if($vname == '') {
		
		$vvalidate++;
			
		}
		
	/************************************************************************/		
	$vsurname = trim($_POST['txtsurname']);
	if($vsurname == '') {
			
			$vvalidate++;
				
			}
			
	/****************************************************************************/				
	$vemail = strtolower(trim($_POST['txtemail']));
	if ($vemail != '') {
	
	$vemail = filter_var($vemail, FILTER_SANITIZE_EMAIL);

	if($vemail == '') {
		
		$vvalidate++;
			
		}
	
	} else {
		
		$vvalidate++;
		
		}

	/**************************************************************************************/	
	$vusername = sha1($vemail);
	$vpassword = trim($_POST['txtpassword']);	
	$vpasswordold = $_POST['txtpasswordencrypt'];
		
	if ($vpassword != '') {
		
		$vpassword = filter_var($vpassword, FILTER_SANITIZE_STRING);
		
		if ($vpassword != '') {
			
				$vpassword = sha1($vpassword);
				
			} else {
				
					$vvalidate++;
					
					}
	} else{
		$vpassword = $vpasswordold;
		}
	
	$vpasswordconfirm = trim($_POST['txtpasswordconfirm']);	
	/**************************************************************************************/	
	$vqstr = "?k1=f";
	$vqstr .= "&k2=" . urlencode($vname);
	$vqstr .= "&k3=" . urlencode($vsurname);
	$vqstr .= "&k4=" . urlencode($vemail);
	$vqstr .= "&k5=" . urlencode($vpassword);
	$vqstr .= "&k6=" . urlencode($vpasswordconfirm);
	$vqstr .= "&txtid=" . urlencode($vid);
	
	if($vvalidate != 0){
		
		header('Location: admin-edit.php?kval=fail' . $vqstr);
		exit();
				
		} else{
			
			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE SQL STATEMENT
			$sql_update = sprintf("UPDATE tbladmin SET cname = %s, csurname = %s, cemail = %s, cusername = %s, cpassword = %s WHERE cid = %u",
				escapestring($vconnection, $vname, 'text'), 
				escapestring($vconnection, $vsurname, 'text'), 
				escapestring($vconnection, $vemail, 'text'),
				escapestring($vconnection, $vusername, 'text'),
				escapestring($vconnection, $vpassword, 'text'),
				escapestring($vconnection, $vid, 'int') 
				);

				$update_result = mysqli_query($vconnection, $sql_update);
			
				if($update_result){
					$_SESSION['svadminaccess'] == $vaccess;
					header('Location: admin-display.php');
					exit();
					
					}else{
						
						header('Location: admin-edit.php?kid=' . $vid . '&kupdate=false');
						exit();
						
						}			
					}	

	}else{
		
		header('Location: signout.php');
		exit();
	
	}
?>