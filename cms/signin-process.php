<?php session_start(); ?>
<?php 

//CHECK IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
	//SET A BASELINE VALUE FOR VALIDATION CHECK
	$vvalidate = 0;
	$vstatus = 'a';
	
	//EXTRACT ALL THE VALUES FROM THE GET SUPER GLOBAL ARRAY AND ASSIGN THEM TO GLOBAL VARIABLES
	$vusername = trim($_POST['txtusername']); 
	if ($vusername != '') {
		$vusername = filter_var($vusername, FILTER_SANITIZE_EMAIL);
		if ($vusername != '') {
			if (filter_var($vusername, FILTER_VALIDATE_EMAIL)) {
				$vusername = sha1($vusername);
				} else {
					$vvalidate++;
					}
		}
	} 	
		
	$vpassword = trim($_POST['txtpassword']);	
	if ($vpassword != '') {
		$vpassword = filter_var($vpassword, FILTER_SANITIZE_STRING);
		if ($vpassword != '') {
				$vpassword = sha1($vpassword);
			} else {
					$vvalidate++;
					}
		} 	
	
	if ($vvalidate > 0 ) {
		
		session_destroy();
		header('Location: signin.php?kvalidate=failed');
		exit();
		
		} else {
			
			//CONNECT TO THE MYSQL SERVER
			require('inc-connection.php');
			
			//CALL IN THE FUNCTION ESCAPE STRING()
			require('inc-function-escapestring.php');
			
			//FORMULATE A SQL STATEMENT AND ASSIGN THE OUTCOME TO A VARIABLE
			$sql_signin = sprintf("SELECT * FROM tbladmin WHERE cusername = %s AND cpassword = %s",
			escapestring($vconnection, $vusername, 'text'),
			escapestring($vconnection, $vpassword, 'text')
			);
			
			//EXECUTE THE SQL STATEMENT
			$rs_signin = mysqli_query($vconnection, $sql_signin);
			
			//CREATE AN ASSOCIATIVE ARRAY OF THE RECORD SET 
			$rs_signin_rows = mysqli_fetch_assoc($rs_signin);	
			
			//CLOSE CONNECTION
			mysqli_close($vconnection);
			
			//COUNT THE NUMBER OF RECORDS RETURNED BY THE RECORD SET
			$rs_signin_total_records = mysqli_num_rows($rs_signin);
		
				if ($rs_signin_total_records == 1){
			
					//EXTRACT FROM THE ASSOCIATIVE ARRAY THE VALUE ASSOCIATED WITH THE KEY aname
					$_SESSION['svadminid']= $rs_signin_rows['cid'];
					$_SESSION['svadmindatetime']= $rs_signin_rows['cdatetime'];
					$_SESSION['svadminname']= $rs_signin_rows['cname'];
					$_SESSION['svadminsurname']= $rs_signin_rows['csurname'];
					$_SESSION['svadminemail']= $rs_signin_rows['cemail'];
					
				header('Location: cms-homepage.php');
				exit();
				
				} else {
					header('Location: signin.php?ksignin=failed');
					exit();
					}
			}
			
	} else {
		header('Location: signout.php');
		exit();
		}
?>