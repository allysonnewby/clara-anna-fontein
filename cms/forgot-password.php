<?php session_start() ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<!DOCTYPE HTML>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- This piece of valid code tells mobile devices not to zoom out as a default. -->
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    
	<title>Forgot Password</title>

    <link href="../stylesheets/html_css_reset.css" rel="stylesheet" type="text/css">
	<link href="../stylesheets/public.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/cssmain.css" rel="stylesheet" type="text/css">
	<?php require('inc-cms-head-content.php'); ?>
</head>

<body>
	<!-- WRAPPER -->
    <div id="wrapper">
		<!-- CONTENT CONTAINER -->
		<div id="content_container">
        	<img src="../images/caf-logo.jpg" id="logo">
            <h1>Forgot Password</h1>
            
            <!--AFTER PASSWORD FORGET MAIL SENT SUCESSFULLY-->
			<?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'sent'){ ?>
                <div class="error">An Email has been sent with instructuctions to reset your password.</div>
            <?php } ?>
            <!--AFTER PASSSORD FORGET MAIL NOT SENT-->
			<?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'failed'){ ?>
                <div class="error">Sorry, there was a problem, please try again.</div>
            <?php } ?>
            
            <?php if(isset($_GET['kpassword']) && $_GET['kpassword'] === 'reset'){ ?>
                <div class="error">Password has been reset. Please sign in with your new password.</div>
                <br>
       		<?php } ?>
            
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                  <div class="msg_box">Please fill in the required fields!</div>
            <?php } ?>     
                  
            <form method="post" action="forgot-password-process.php">
                <br>
                <label class="required">Email</label>
                <input type="text" name="txtemail" placeholder="Enter email">
                <br><br>
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>"> 
                <input type="submit" name="btnsignin" value="Send" >
            </form>
            <br>
            <p id="p1"><a href="signin.php">Back to signin</a></p>
         </div>
   
    </div>
</body>
</html>


