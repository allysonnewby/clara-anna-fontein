<?php require("inc-cms-pre-doctype.php"); ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php

$vid = $_REQUEST['txtid'];

require('inc-connection.php');

$sql_news = "SELECT * FROM tblnews WHERE nid = $vid";

$rs_news = mysqli_query($vconnection, $sql_news);

$rs_news_rows = mysqli_fetch_assoc($rs_news);
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div class="msg">Value required!</div>';}		
	}
	
//FUNCTION@
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!DOCTYPE HTML>
<html>

<head>
<?php require("inc-cms-head-content.php"); ?>
<script src="ckeditor/ckeditor.js"></script>
<script>
function toolbar(x){
	CKEDITOR.replace(x, {
		on: 
		{
			instanceReady : function(ev)
			{
				//Output paragraphs as <p>Text</p>
				this.dataProcessor.writer.setRules('p',
					{
						indent: false,
						breakBeforeOpen: false,
						breakAfterOpen: false,
						breakBeforeClose: false,
						breakAfterClose: false
					});
				}
			}
		});
}
</script>


</head>

<body>

<div id="main_container">

    <div id="branding_bar">
    <?php require("inc-cms-branding-bar.php"); ?>
    </div>
    
    <div id="body_column_left_container">
    
        <div id="body_column_left">
            <?php require("inc-cms-accordion_menu.php"); ?>
        </div>
        
    </div>
    
    <div id="body_column_right_container">
        
        <div id="body_column_right">
            <h2>News</h2>
               
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                <div class="msg_box">Please fill in the required fields!</div>
            <?php } ?>   
             
            <form method="post" action="news-edit-process.php" class="frmtekiah" enctype="multipart/form-data">
               <!--REQUIRED FIELDS-->
                <fieldset>
                    <legend>Edit</legend>
                    
                        <label class="required">Title</label>
                        <?php echo valrequired_warning('k2'); ?>
                        <input type="text" name="txttitle" autofocus value="<?php if(isset($_GET['k2']) && $_GET['k2'] == ''){echo valissue('k2');}else{ echo $rs_news_rows['ntitle'];}  ?>"><br><br>
                        
                        <label>Description</label>
                        <input type="text" name="txtdescription" value="<?php if(isset($_GET['k3']) && $_GET['k3'] == ''){echo valissue('k3');}else{ echo $rs_news_rows['ndescription'];} ?>"><br><br>

                        <label>Content</label><br>
                        <textarea name="txtcontent"><?php if(isset($_GET['k5']) && $_GET['k5'] == ''){echo valissue('k5');}else{ echo $rs_news_rows['ncontent'];} ?></textarea>
                        <script>toolbar('txtcontent');</script>
						<br>
                        
						<?php if(isset($_GET['key']) && $_GET['key'] === 'notanimage'){ ?>
                            <div class="msg_box">The file you selected is not an image</div>
                        <?php } ?>
                        
						<?php if(isset($_GET['key']) && $_GET['key'] === 'filenotsupported'){ ?>
                            <div class="msg_box">This file extension is not supported, please only upload a JPEG or JPG</div>
                        <?php } ?>   
                        <label>Current image</label><br>
                        <img class="curimg" src="../uploaded-images-news/<?php echo $rs_news_rows['nimage']; ?>"><br>
                        
                        <label>Upload an new image</label><br>
                        <input type="file" name="txtimglarge">
                    
                        <input type="hidden" name="txtid" value="<?php echo $rs_news_rows['nid'];?>"><br><br>
                        <input type="submit" name="btnsubmit" value="Edit"> 
                </fieldset> 
                
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>">   
                <input type="hidden" name="txtimgold" value="<?php echo $rs_news_rows['nimage']; ?>">   
            </form>            
    
        </div>
            
    </div>
    
    <div class="clearfloat_both"></div>
                
</div>


</body>
</html>