<?php session_start() ?>
<?php 
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];

$vemail = $_GET['kemail'];
?>
<!DOCTYPE HTML>
<html>

<head>
	<?php require('inc-cms-head-content.php');?>    
    <link href="../stylesheets/html_css_reset.css" rel="stylesheet" type="text/css">
	<link href="../stylesheets/public.css" rel="stylesheet" type="text/css">
    <link href="stylesheets/cssmain.css" rel="stylesheet" type="text/css">

</head>

<body>
	<!-- WRAPPER -->
    <div id="wrapper">
		<!-- CONTENT CONTAINER -->
		<div id="content_container">
        	<img src="../images/caf-logo.jpg" id="logo">
            <h1>Update Password</h1>
            <p>Enter a new password</p>
            <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
                  <div class="msg_box">Please fill in the required fields!</div>
            <?php } ?>     
                  
            <form method="post" action="update-password-process.php">
                <br>
                <label class="required">Password</label>
                <input type="password" name="txtpassword" placeholder="Password">
                <br><br>
                <label class="required">Confirm Password</label>
                <input type="password" name="txtpasswordconfirm" placeholder="Confirm Password">
                <br><br>
                <input type="hidden" name="txtemail" value="<?php echo $vemail; ?>">
                <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity; ?>"> 
                <input type="submit" name="btnsignin" value="Update" >
            </form>
            <br>

         </div>
         <p id="p1"><a href="signin.php">Back to signin</a></p>
    </div>
</body>
</html>


