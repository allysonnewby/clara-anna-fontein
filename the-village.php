<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>The Village | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">OM DIT TE VERSTAAN MOET JY DIT SIEN. DIT BEPROEF. DIT GENIET.</h1>
    <h4 class="cinzel jules">- ANONIEM</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/village-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/village-2.jpg" alt="" id="logo"/>
    </figure>
    
    <div id="slidy-container">
        <p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi3.jpg" alt=""/>
            <img src="images/cgi2.jpg"  alt=""/>
        </figure>
        <a href="register-townhouses.php"><img src="images/register-your-interest.png" id="red-banner" alt="Phase 2 on show now"></a>
        <?php require('butterfly.php');?>
        </div>

    
    <div class="clear_float"></div>
</div>


<div class="main_heading heading-2">
	<h1 class="title title-mid">"DAAR IS GEEN HEMEL OP AARDE, MAAR DAAR IS STUKKIES DAARVAN."</h1>
    <h4 class="cinzel jules">- JULES RENARD</h4>
</div>

<section class="section-middle">
	<aside class="aside-1">
    	<figure>
        	<img src="images/village-couple.jpg" class="boxshadow">
        </figure>
    	<article class="top-article">
        	<p class="quicksand">Finding the right house to call home and raise a family is exciting, but sifting through the mess of fixer-uppers is a tedious task – that’s why we suggest The Village @ Clara Anna Fontein – a young family-oriented community that offers all the housing facilities and amenities needed to help kick-start a comfortable and safe life for you and your family. </p>
            <p class="quicksand">The Village @ Clara Anna Fontein is the perfect ready-to-move-in, lock-up-and-go home with great facilities like an on-site private school and fresh air living for the whole family! </p>
        </article>
    </aside>

	<aside class="aside-2">
    
    <article class="section-m-aside2-article">
    	<p class="bilbo middle-p village-middle-p">Are you a young professional or perhaps a newly married young couple ready to start that little family of your own, but having trouble finding that perfect home? </p>
    </article>
    
    <figure class="village-landscape">
        <img src="images/village-landscape.jpg">
    </figure>
    </aside>
    
    <div class="clear_float"></div>
</section>

<section class="section-bottom">
	<aside class="aside-1-bottom">
    	<figure id="bottom-section-main-fig" class="bottom-fig">
        	<img src="images/village-cgi.jpg">
        </figure>
        
        
        <div class="clear_float"></div>
    </aside>

	<aside class="aside-2-bottom">
 	   <article class="bottom-article">
            <p class="bilbo coffee village-dog-p">So, before you pack your bags and give word to the kids, register your interest today, and we’ll get back to you in a hurry! </p>
        </article>
    
    </aside>
	<div class="clear_float"></div>
</section>

<section class="section-bottom village-bottom-section">
    <figcaption class="facilities-heading">
        <h1 class="title title-mid">"I LIKE THIS PLACE AND COULD WILLINGLY WASTE MY TIME IN IT"</h1>
        <h4 class="cinzel jules">William Shakespeare</h4>
    </figcaption>
    <figure class="facilities">

        <img src="images/village-footer.jpg">
    </figure>
</section>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('VILLAGE')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('VILLAGE')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Village')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Village')").addClass("current-menu-item-a");
});

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
