<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Expires" content="0"> 
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">

<link href='https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Cinzel' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Bilbo' rel='stylesheet' type='text/css'>

<!--TEACHES OLDER BROWSERS HTML5-->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--IE BUG FIX-->
<!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!-- 
The following script allows all browsers to support media queries in CSS.
-->
<!--[if lt IE 9]>
<script src="http://css3-mediaqueriesjs.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<!--END (TEACHES OLDER BROWSERS HTML5)-->
<!--[if gt IE 6]>
<style>#butterfly {display: none;}</style>
<![endif]-->

<link rel="stylesheet" type="text/css" href="stylesheets/html_css_reset.css">
<link rel="stylesheet" type="text/css" href="stylesheets/public.css">

<!-- FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

   