<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<link rel="stylesheet" type="text/css" href="facility.css">
<title>On Site Facilities | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<div class="main_heading">
	<h1 class="title title-top">OM DIT TE VERSTAAN MOET JY DIT SIEN. DIT BEPROEF. DIT GENIET.</h1>
    <h4 class="cinzel jules">- ANONIEM</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/on-site-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/on-site-2.jpg" alt="" id="logo"/>
    </figure>
   
    <div id="slidy-container">
        <p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi2.jpg" alt=""/>
            <img src="images/cgi5.jpg"  alt=""/>
        </figure>
        <img src="images/short-red-flash-home.png" id="red-banner" alt="Phase 2 on show now">
        <?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"ONS GROOTSTE NATUURLIKE BRON IS DIE INTELLEK VAN ONS KINDERS"</h1>
    <h4 class="cinzel jules">- Walter Elias Disney</h4>
</div>
    
<section class="section-middle iexplorer" id="ie">
    <p class="bilbo facilities-par">At Clara Anna Fontein you'll effortlessly achieve a well-balanced life, one that fulfills mind, body and soul. Take a look...</p>
    
        <figure class="flip-container" id="ideal-location" >
            <img src="images/ideal-location.jpg">
        </figure>

        <div class="flip-container-ie" id="fibre-to-home-connectivity">
            <img src="images/fibre-to-home-connectivity.jpg">
        </div>

        <figure class="flip-container" id="manor-house">
             <img src="images/manor-house.jpg">
        </figure>

        <figure class="flip-container" id="reddam-private-school">
            <img src="images/reddam-private-school.jpg">
        </figure>
    
        <figure class="flip-container" id="bird-watching">
            <img src="images/bird-watching.jpg">   
        </figure>
    
        <figure class="flip-container" id="function-venues">
            <img src="images/function-venues.jpg">
        </figure>
    
        <figure class="flip-container" id="game-viewing">
            <img src="images/game-viewing.jpg">
        </figure>
            
        <figure class="flip-container" id="play-parks">
            <img src="images/play-parks.jpg">
        </figure>
    
        <div class="flip-container" id="outdoor-pool">
            <img src="images/outdoor-pool.jpg">
        </div>  
            
        <div class="flip-container" id="jogging-walking-trails">
            <img src="images/jogging-walking-trails.jpg">
        </div>
           
        <div class="flip-container" id="state-of-the-art-security">
            <img src="images/state-of-the-art-security.jpg">
        </div>
            
        <div class="flip-container" id="private-meeting-rooms">
            <img src="images/private-meeting-rooms.jpg">
        </div>
            
        <div class="flip-container" id="gym">
            <img src="images/gym.jpg">
        </div>    
        
        <div class="flip-container" id="squash-tennis-courts">
            <img src="images/squash-tennis-courts.jpg">
        </div>   
        
        <div class="flip-container" id="mountain-views">
            <img src="images/mountain-views.jpg">
        </div>
    
    <div class="clear_float"></div>
</section>
    
    
<section class="section-bottom">
    <figure class="facilities footer-facilites">
        <img src="images/ZEBRAS.jpg">
    </figure>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('FACILITIES')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('FACILITIES')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Facilities')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Facilities')").addClass("current-menu-item-a");
    
    var height = $('.front-ie img').height();
    $('.back-ie').css("height", height);
    
    $('.flip-container').mouseenter(function(){
        
        var id = $(this).attr("id");
        
        var newsrc = 'images/' + id + '-overlay.jpg';
        
        $(this).children(':first').attr('src', newsrc); 
    });
    
        $('.flip-container').mouseleave(function(){
        
        var id = $(this).attr("id");
        
        var newsrc = 'images/' + id + '.jpg';
        
        $(this).children(':first').attr('src', newsrc); 
    });

});

</script>    
</body>
<?php require('detect-ie.php');?>
</html>
