<?php session_start();?>
<?php 

//CHECK .IF FORM WAS SUBMITTED
if (isset($_POST['txtsecurity']) && $_POST['txtsecurity'] === $_SESSION['svadminsecurity']) {
	
//SET A BASELINE VALUE FOR VALIDATION CHECK
$vvalidate = 0;
$vdate = date('Y-m-d h:i:s');
$vvalidemail = 0;
$vvalidtel = 0;    
$vvalidcell = 0;    
//COLLECT ALL VALUES FROM THE FORM & VALIDATE OR SANITIZE IF NECESSARY

//REQUIRED
$vname = filter_var(trim($_POST['txtname']), FILTER_SANITIZE_STRING);
if ($vname === '') {

    $vvalidate++;

}

$vsurname = filter_var(trim($_POST['txtsurname']), FILTER_SANITIZE_STRING);
if ($vsurname === '') {

    $vvalidate++;

}
/************************************************************************************/    
$vlandline = filter_var(trim($_POST['txtlandline']), FILTER_SANITIZE_STRING);

if ($vlandline === '') {

    $vvalidate++;

}else if (strlen($vlandline) < 10) {

    $vvalidtel++;

    } else if(substr($vlandline,0,1) !== '0'){
        $vvalidtel++;
        }   
/************************************************************************************/   
$vcell = filter_var(trim($_POST['txtcell']), FILTER_SANITIZE_STRING);
if ($vcell === '') {

    $vvalidate++;

}else if (strlen($vcell) < 10) {

    $vvalidcell++;

    } else if(substr($vcell,0,1) !== '0'){
        $vvalidcell++;
        }
    
    
/************************************************************************************/
$vemail = filter_var(trim($_POST['txtemail']), FILTER_SANITIZE_EMAIL);
if ($vemail === '') {

    $vvalidate++;

} else if(filter_var($vemail, FILTER_VALIDATE_EMAIL) === false){
    $vvalidemail++;
    
}  
    
/************************************************************************************/
$vdob = trim($_POST['txtdob']);
if ($vdob === '') {

    $vvalidate++;

}    
/************************************************************************************/
$vinterest = $_POST['txtinterest']; 
if ($vinterest === '') {

    $vvalidate++;

} 
$vbedrooms = $_POST['txtbedrooms'];
if ($vbedrooms === '') {

    $vvalidate++;

}     
$vbathrooms = $_POST['txtbathrooms']; 
if ($vbathrooms === '') {

    $vvalidate++;

}     
/************************************************************************************/
if($vvalidate > 0){
   $vqstr = "?k1=f";
    $vqstr .= "&k2=" . urlencode($vname);
}else{
    $vqstr = "?k2=" . urlencode($vname);
}


$vqstr .= "&k3=" . urlencode($vsurname);
$vqstr .= "&k4=" . urlencode($vlandline);
$vqstr .= "&k5=" . urlencode($vcell);
$vqstr .= "&k6=" . urlencode($vemail);
$vqstr .= "&k7=" . urlencode($vdob); 
$vqstr .= "&k8=" . urlencode($vinterest);
$vqstr .= "&k9=" . urlencode($vbedrooms);
$vqstr .= "&k10=" . urlencode($vbathrooms);

if($vvalidemail > 0){
   $vqstr .= "&kemail=invalid";
}
    
if($vvalidtel > 0){
    $vqstr .= "&ktel=invalid";   
}    
   
if($vvalidcell > 0){
    $vqstr .= "&kcell=invalid";   
}    
       
if($vvalidate !== 0 || $vvalidtel !== 0 || $vvalidemail !== 0 || $vvalidcell !== 0 ){

    header('Location: register-townhouses.php' . $vqstr);
    exit();

    }else{

        //Email Confirmation to user
        $vto = 'allyson@admakers.com';

        //Subject
        $vsubject = "Clara Anna Fontein - Registered Interest in Townhouse";

        //Message
        $vmessage = '
        <html>
            <head>
                <title>Clara Anna Fontein - Registered Interest in Townhouse</title>
            </head>
            <body>
                <table style="background-color:#FEFEFE; font-family: Arial, Verdana, Tahoma; font-size: 14px; letter-spacing: 0.03em; word-spacing: 0.2em; line-height: 1.6em;" cellspacing="0" cellpadding="0" width="600">
                    <tr>
                        <td style="padding: 6px;">
                            <p>Full Names: <br>'. $vname .'</p>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 6px;">
                            <p>Surname: <br>'. $vsurname .'</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="padding: 6px;">
                            <p>Landline: <br>'. $vlandline .'</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="padding: 6px;">
                            <p>Cellphone: <br>'. $vcell .'</p>
                        </td>
                    </tr>                    
 
                    <tr>
                        <td style="padding: 6px;">
                            <p>Email: <br>'. $vemail .'</p>
                        </td>
                    </tr> 

                    <tr>
                        <td style="padding: 6px;">
                            <p>Date of Birth: <br>'. $vdob .'</p>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 6px;">
                            <p>Interested in: <br>'. $vinterest .'</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="padding: 6px;">
                            <p>Rooms: <br>'. $vbedrooms .'</p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="padding: 6px;">
                            <p>Bathrooms: <br>'. $vbathrooms .'</p>
                        </td>
                    </tr>                    
                    <tr>
                        <td style="padding: 6px;">
                            <p>--</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 6px;">
                            <p>This e-mail was sent from the Register Interest in a Townhouse form on Clara Anna Fontein (http://claraannafontein.co.za)</p>
                        </td>
                    </tr>
                    
                </table>
            </body>
        </html>';

        //To send HTML mail, you can set the Content-type header.
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

        //Additional headers
        $headers .= "From: ".$vname . " " . $vsurname . "<" . $vemail. "> \r\n";

        //Init mail
        $vemailsent = mail($vto, $vsubject, $vmessage, $headers);

        //END OF MAIL FUNCTION

        if($vemailsent) {
            
            //Email Confirmation to user
            $vrto = $vemail;

            //Subject
            $vrsubject = "Clara Anna Fontein - Registered Interest in Townhouse";

            //Message
            $vrmessage = '
            <html>
                <head>
                    <title>Clara Anna Fontein - Registered Interest in Townhouse</title>
                </head>
                <body>
                    <table style="background-color:#FEFEFE; font-family: Arial, Verdana, Tahoma; font-size: 14px; letter-spacing: 0.03em; word-spacing: 0.2em; line-height: 1.6em;" cellspacing="0" cellpadding="0" width="600">
                        <tr>
                            <td style="padding: 6px;">
                                <p>Thank you, we confirm receipt of your interest in The Village @ Clara Anna Fontein (Townhouses) which will be launching mid-April 2016. Our office will make contact with you in the beginning of April to confirm further details, prices and date of launch.</p>
                            </td>
                        </tr>

                        <tr>
                            <td style="padding: 6px;">
                                <p>Clara Anna Fontein</p>
                            </td>
                        </tr>

                    </table>
                </body>
            </html>';

            //To send HTML mail, you can set the Content-type header.
            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

            //Additional headers
            //$headers .= "From: ".$vname . " " . $vsurname . "<" . $vemail. "> \r\n";

            //Init mail
            $vremailsent = mail($vrto, $vrsubject, $vrmessage, $headers);
            if($vremailsent) {
                header('Location: register-townhouses.php?kemail=sent');
                exit();
            }
            
        } else {

            header('Location: register-townhouses.php?kmail=failed');
            exit();	

        }
    }

}else{
		
	header('Location: register-townhouses.php?kval=f');
	exit();
	
	}
?>