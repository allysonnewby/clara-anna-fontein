<?php 
session_start();
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div style="color: #F00" class="msg quicksand">Value required!</div>';}		
	}
	
//FUNCTIONS
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Register | Clara Anna Fontein</title>
   
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">REGISTER</h1>
        
    </div>
    <div class="clear_float"></div>

</div>
  
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">REGISTER</h1>
        
    </div>
    
    <div class="clear_float"></div>
    
</div>    
   
<section class="section-middle-register">
    <h2 class="quicksand-heading">Townhouses</h2>
    <p class="quicksand">The townhouses will be launched during April/May 2016, should you wish to receive more information, please complete the following registration form to ensure your personal details are on our database:</p>
    <a href="register-townhouses.php" class="quicksand register-a">Register</a>
    <br>
    <h2 class="quicksand-heading" style="margin-top: 4%;">Retirement</h2>
    <p class="quicksand">The retirement village will be launched mid to end of 2016, to receive more information, complete the registration form below to ensure your personal details are on our database:</p>
    <a href="register-retirement.php" class=" quicksand register-a">Register</a>
</section>


</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){

    var container_width = $(".main-container-contact-wide").width(); 
    var container_height = $(".main-container-contact-wide").height(); 
    
    var percentageoff = (3/100 * container_width)*2;
    
    var new_height = container_height - percentageoff;

    $(".contact-heading").css("height", new_height);
    $(".contact-title").css("line-height", new_height + 'px');
});

</script>
</body>
</html>
