<?php

//CREATE SQL STATEMENT
$sql_docs = "SELECT * FROM tbldocs ORDER BY pdatetime ASC";

//CONNECT TO MYSQL SERVER
require('cms/inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_docs = mysqli_query($vconnection, $sql_docs);

//CREATE AN ASSOCIATIVE ARRAY
$rs_docs_rows = mysqli_fetch_assoc($rs_docs);

$rs_total_docs = mysqli_num_rows($rs_docs);
?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Documents | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/documents-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/documents-2.jpg" alt="" id="logo"/>
    </figure>
    
    <div id="slidy-container">
        <p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi3.jpg" alt=""/>
            <img src="images/cgi1.jpg"  alt=""/>
        </figure>
        <img src="images/short-red-flash-home.png" id="red-banner" alt="Phase 2 on show now">
        <?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"OUR GREATEST NATURAL RESOURCE IS THE MINDS OF OUR CHILDREN."</h1>
    <h4 class="cinzel jules">- Walter Elias Disney</h4>
</div>

<section class="section-middle docs-section">
    
    <?php do{?>
    <div class="docs">
        <p class="docs-p"><?php echo $rs_docs_rows['ptitle']; ?></p>
        <a target="_blank"class="docs-a" href="uploaded-pdfs/<?php echo $rs_docs_rows['ppdf']; ?>">DOWNLOAD HERE</a>
    </div>
    
    <?php }while($rs_docs_rows = mysqli_fetch_assoc($rs_docs))?>
    
    <div class="clear_float"></div>
</section>

<section class="section-bottom">

	<div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('DOCUMENTS')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('DOCUMENTS')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Documents')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Documents')").addClass("current-menu-item-a");
});

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
