<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Home | Clara Anna Fontein</title>
<script>
$(document).ready(function(){
	
	$("#hamburger").click(function(){
		$(".mobile_nav").slideToggle();
	});
});
</script>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>


<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/home-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/home-2.jpg" alt="" id="logo"/>
    </figure>
    
    <div id="slidy-container">
        <p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi4.jpg" alt=""/>
            <img src="images/cgi2.jpg"  alt=""/>
        </figure>
        <img src="images/short-red-flash-home.png" id="red-banner" alt="Phase 2 on show now">
        <?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<div class="main_heading heading-2">
	<h1 class="title title-mid">"ON EARTH THERE IS NO HEAVEN, BUT THERE ARE PIECES OF IT."</h1>
    <h4 class="cinzel jules">- JULES RENARD</h4>
</div>

<section class="section-middle">
	<aside class="aside-1">
    	<figure>
        	<img src="images/caf-landscape-boxshadow.png" class="boxshadow">
        </figure>
    	<article class="top-article">
        	<p class="quicksand">Consisting of 344 single, residential erven of generous proportions, mostly with spectacular views, Clara Anna Fontein offers residents an enviable and much sought-after lifestyle.</p>
            <p class="quicksand">The existing 17th century manor house is being restored to its original splendour and will offer a variety of luxurious facilities, allowing residents to relax and spend time with family and friends. A new fully-equipped sports centre with a squash court, two tennis courts, a swimming pool and change rooms will be included in the development.</p>
        </article>
        <figure class="lizard">
        	<img src="images/lizard.png">
        </figure>
    </aside>

	<aside class="aside-2">
    
    <article class="section-m-aside2-article">
    	<p class="bilbo middle-p">Just a 20 min drive from the Cape Town CBD, you will discover the enchanting Clara Anna Fontein - an exclusive lifestyle estate where contemporart living blends harmoniously with large tracts of lush open areas, protected wetlands and a private game reserve.</p>
    </article>
    
    <figure id="img-1">
    	<img src="images/caf-damshot-sliced-1.jpg" >
    </figure>
    <figure id="img-2">    
        <img src="images/caf-child.jpg" >
    </figure> 
    <figure id="img-3" class="last">   
        <img src="images/caf-damshot-sliced-2.jpg" >
    </figure>
    </aside>
    
    <div class="clear_float"></div>
</section>

<section class="section-bottom">
	<aside class="aside-1-bottom">
    	<figure id="bottom-section-main-fig" class="bottom-fig">
        	<img src="images/caf-viewshot-web.jpg">
            <figcaption class="bilbo figcap-main">outstanding views...</figcaption>
        </figure>
        <figure id="bottom-section-sub-1" class="bottom-fig">
        	<img src="images/movie-image.jpg">
        </figure>
        <figure id="bottom-section-sub-2" class="bottom-fig">
        	<img src="images/children-playing.jpg">
            <figcaption class="bilbo figcap-sub">Rinkink in die son!</figcaption>
        </figure>
        
        <div class="clear_float"></div>
    </aside>

	<aside class="aside-2-bottom">
 	   <article class="bottom-article">
        	<p class="quicksand first">All internal roads will be paved and residents can enjoy beautifully landscaped parks, a private dam and a lookout hide for game and bird spotting.</p>
            <p class="quicksand">Families will also be well catered for, with a private school adjacent to the scenic estate, as well as retail and other convenience amenities a few minutes away.</p>
            <p class="quicksand">Clara Anna Fontein will enjoy state-of-the-art security, including electrified perimeter fencing, CCTV cameras on the perimeter and within the estate, 24 hour-manned security and biometric finger print access.</p>
            <p class="quicksand">Managed by a full-time Estate Manager, Clara Anna Fontein will offer residents sustenance for the soul and peace of mind.</p>
            <p class="bilbo coffee">...and there's still time for a morning coffee before work!</p>
        </article>
    
    </aside>
	<div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('HOME')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('HOME')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('HOME')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('HOME')").addClass("current-menu-item-a");
});

</script>
<?php require('detect-ie.php');?>    
</body>
</html>
