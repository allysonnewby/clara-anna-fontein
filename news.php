<?php
$per_page = 8;

if(isset($_GET['page'])){
	$page = $_GET['page'];
	}else{
		$page = 1;
		}

//pAGE WILL START FROM  0 and multiply by per page
$start_from = ($page-1) * $per_page;

//CREATE SQL STATEMENT
$sql_news = "SELECT * FROM tblnews WHERE nstatus NOT LIKE 'd' AND nstatus = 'a' ORDER BY ndatetime DESC LIMIT $start_from, $per_page";

//CONNECT TO MYSQL SERVER
require('cms/inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_news = mysqli_query($vconnection, $sql_news);

//CREATE AN ASSOCIATIVE ARRAY
$rs_news_rows = mysqli_fetch_assoc($rs_news);

$rs_news_total_rows = mysqli_num_rows($rs_news);
?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>News | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid">NEWS</h1>
        
    </div>
    <div class="clear_float"></div>
</div>
    
    
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid">NEWS</h1>
        
    </div>
    <div class="clear_float"></div>
</div>    

<section class="section-middle">
    <?php if($rs_news_total_rows !== 0 ){ ?>
    <?php do{ ?>
        <article class="news-article">
            <figure class="news-img">
                <img src="uploaded-images-news/<?php if($rs_news_rows['nimage'] != 'na'){echo $rs_news_rows['nimage'];}else{echo 'caf-logo.jpg';} ?>">
            </figure>
            <div class="news-content">
                <a href="news-details.php?kid=<?php echo $rs_news_rows['nid']; ?>"><h1 class="open-san"><?php echo $rs_news_rows['ntitle']; ?></h1></a>
                <?php if($rs_news_rows['ndescription'] != 'na'){?><p class="quicksand description-p"><?php echo $rs_news_rows['ndescription']; ?></p><?php }?>
                <p class="date-added">Date Added: <?php echo $rs_news_rows['ndatetime']; ?></p>
            </div>
            <div class="clear_float"></div>
        </article>           
    <?php }while($rs_news_rows = mysqli_fetch_assoc($rs_news)) ?>
    <?php }else{
             echo '<p class="bilbo">No News Currently</p>';      
            }?>
    <div class="clear_float"></div>
</section>

<section class="section-bottom">

	<div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('NEWS')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('NEWS')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('News')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('News')").addClass("current-menu-item-a");
    
    var container_width = $(".main-container-contact-wide").width(); 
    var container_height = $(".main-container-contact-wide").height(); 
    
    var percentageoff = (3/100 * container_width)*2;
    
    var new_height = container_height - percentageoff;

    $(".contact-heading").css("height", new_height);
    
});

</script>
</body>
</html>
