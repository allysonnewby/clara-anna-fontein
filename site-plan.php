<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Site Plan | Clara Anna Fontein</title>
<link rel="stylesheet" type="text/css" href="stylesheets/siteplan.css">    
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">OM DIT TE VERSTAAN MOET JY DIT SIEN. DIT BEPROEF. DIT GENIET.</h1>
    <h4 class="cinzel jules">- ANONIEM</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid">SITE PLAN</h1>
        
    </div>
    <div class="clear_float"></div>

</div>
  
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid">SITE PLAN</h1>
        
    </div>
    <div class="clear_float"></div>
</div>     
    
<section class="section-middle">
    <figure class="site-plan">
        <img src="images/site-plan-map.jpg">
        
        <!-- HOTSPOTS - GATE HOUSE -->
        <a href="javacript:void()"><img src="images/pin.png" class="pin gate-house"></a>
        <figure class="gate-house-img hotspot-img">
            <h2 class="quicksand-heading">Gate House</h2>
            <img src="images/cgi-front-gate.jpg" >
        </figure>
        
        <!-- HOTSPOTS - LIFESTYLE CENTER -->
        <a href="javacript:void()"><img src="images/pin.png" class="pin lifestyle"></a>
        <figure class="lifestyle-img hotspot-img">
            <h2 class="quicksand-heading">Lifestyle Center</h2>
            <img src="images/cgi-front-gate.jpg" >
        </figure>
        
        <!-- HOTSPOTS - FUNCTION VENUE -->
        <a href="javacript:void()"><img src="images/pin.png" class="pin function-venue"></a>
        <figure class="function-img hotspot-img">
            <h2 class="quicksand-heading">Function Venue</h2>
            <img src="images/cgi-front-gate.jpg" >
        </figure>
        
        <!-- HOTSPOTS - REDDAM HOUSE -->
        <a href="javacript:void()"><img src="images/pin.png" class="pin reddam"></a>
        <figure class="reddam-img hotspot-img">
            <img src="images/reddam-logo.png" >
        </figure>
    </figure>
    
    
    <article>
    <p class="quicksand">Above is a graphic representation of the proposed building footprint, travelways, parking, location of facilities, trails, lighting, landscaping, drives, nature reserves, garden and structural elements that are part of the development.</p>
    <p class="quicksand">With the use of this site plan, you’ll also be able to see how many plots have been sold, and how many stands are still available.
</p>
    </article>
</section>

<section class="section-bottom village-bottom-section">
    <figure>
        <img src="images/site-plan-footer.jpg">
    </figure>
</section>
</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('PLAN')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('PLAN')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Plan')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Plan')").addClass("current-menu-item-a");
    
    //GATE HOUSE IMAGE HOTSPOT
    $('.gate-house').mouseenter(function(){
        $('.gate-house-img').css("display", "block")  
    });
    
    $('.gate-house').mouseleave(function(){
        $('.gate-house-img').css("display", "none")  
    });
    
    //LIFESTYLE IMAGE HOTSPOT
    $('.lifestyle').mouseenter(function(){
        $('.lifestyle-img').css("display", "block")  
    });
    
    $('.lifestyle').mouseleave(function(){
        $('.lifestyle-img').css("display", "none")  
    }); 
    
    //FUNCTION VENUE IMAGE HOTSPOT
    $('.function-venue').mouseenter(function(){
        $('.function-img').css("display", "block")  
    });
    
    $('.function-venue').mouseleave(function(){
        $('.function-img').css("display", "none")  
    }); 
    
    //REDDAM IMAGE HOTSPOT
    $('.reddam').mouseenter(function(){
        $('.reddam-img').css("display", "block")  
    });
    
    $('.reddam').mouseleave(function(){
        $('.reddam-img').css("display", "none")  
    }); 
    
});

</script>
</body>
</html>
