<?php 
session_start();
//GENERATE ENCRYPTED SESSION VARIABLE
$_SESSION['svadminsecurity'] = md5(md5(rand()));
$vsecurity = $_SESSION['svadminsecurity'];
?>
<?php 
//FUNCTION TO DISPLAY GENERAL VALIDATION WARNING
function valrequired_warning($vvariable){
	if(isset($_GET[$vvariable]) && $_GET[$vvariable] == ''){return '<div style="color: #F00" class="msg quicksand">Value required!</div>';}		
	}
	
//FUNCTIONS
function valissue($vvar){
	if(isset($_GET[$vvar]) && $_GET[$vvar] != ''){return  $_GET[$vvar];
	}
}	
?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Contact Us | Clara Anna Fontein</title>
<script src="cms/ckeditor/ckeditor.js"></script>   
<script>
function toolbar(x){
	CKEDITOR.replace(x, {
		on: 
		{
			instanceReady : function(ev)
			{
				//Output paragraphs as <p>Text</p>
				this.dataProcessor.writer.setRules('p',
					{
						indent: false,
						breakBeforeOpen: false,
						breakAfterOpen: false,
						breakBeforeClose: false,
						breakAfterClose: false
					});
				}
			}
		});
}
</script>    
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">CONTACT US</h1>
        
    </div>
    <div class="clear_float"></div>
    <p class="quicksand">For an appointment, contact Johan de Bruyn on 021 976 3180 or 082 881 2011 or <a href="mailto:johan@louwcoet.co.za">johan@louwcoet.co.za</a></p>
</div>
  
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">CONTACT US</h1>
        
    </div>
    
    <div class="clear_float"></div>
    
</div>  
    
    
 <p class="quicksand contact-second-p">For an appointment, contact Johan de Bruyn on 021 976 3180 or 082 881 2011 or <a href="mailto:johan@louwcoet.co.za">johan@louwcoet.co.za</a></p>   
<section class="section-middle-contact section-register">

<form method="post" action="contact-process.php" class="frm">
    <fieldset>
        <?php if(isset($_GET['k1']) && $_GET['k1'] === 'f'){ ?>
            <div class="msg_box quicksand">Please fill in the required fields!</div><br>
        <?php } ?>
        <?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'invalid'){ ?>
            <div class="msg_box quicksand" style="font-size: 90%;">Email address appears to be invalid!</div><br>
        <?php } ?>
        <?php if(isset($_GET['kemail']) && $_GET['kemail'] === 'sent'){ ?>
                <div class="quicksand">Thank you for contacting us.</div><br>
        <?php } ?> 
        <label class="required quicksand">Your Name</label>
        <?php echo valrequired_warning('k2'); ?>
        <input type="text" name="txtname" autofocus value="<?php echo valissue('k2');?>">

        <label class="required quicksand">Email</label>
        <?php echo valrequired_warning('k3'); ?>
        <input type="text" name="txtemail" value="<?php echo valissue('k3');?>">
        
        <label class="required quicksand">Subject</label>
        <?php echo valrequired_warning('k4'); ?>
        <input type="text" name="txtsubject" value="<?php echo valissue('k4');?>">
        
        <label class="required quicksand">Message</label>
        <?php echo valrequired_warning('k5'); ?><br>
        <textarea name="txtmsg"><?php echo valissue('k5');?></textarea>
     
        
        <input type="hidden" name="txtsecurity" value="<?php echo $vsecurity;?>">

        <input type="submit" name="btnsubmit" value="Send">
    </fieldset> 
</form>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3314.7218571867243!2d18.6240203158103!3d-33.81949022393384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1dcc579df2c8516f%3A0x8d2290e25310639d!2sClara+Anna+Fontein!5e0!3m2!1sen!2sza!4v1454677637411"  allowfullscreen></iframe>
    <div class="clear_float"></div>
</section>


</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('CONTACT')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('CONTACT')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Contact')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Contact')").addClass("current-menu-item-a");
    
    var container_width = $(".main-container-contact-wide").width(); 
    var container_height = $(".main-container-contact-wide").height(); 
    
    var percentageoff = (3/100 * container_width)*2;
    
    var new_height = container_height - percentageoff;
    
    $(".contact-heading").css("height", new_height);
    
    $(".contact-title").css("line-height", new_height + 'px');
});

</script>
</body>
</html>
