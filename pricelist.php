<?php session_start(); ?>
<?php
$sql_docs = "SELECT * FROM tbldocs WHERE ptitle = 'PRICE LIST'";

//CONNECT TO MYSQL SERVER
require('cms/inc-connection.php');

//EXECUTE SQL STATEMENT
$rs_docs = mysqli_query($vconnection, $sql_docs);

//CREATE AN ASSOCIATIVE ARRAY
$rs_docs_rows = mysqli_fetch_assoc($rs_docs);


?>
<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Pricelist | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
    
<?php require('mobile-nav.php'); ?>    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container-contact">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">PRICE LIST</h1>
        
    </div>
    <div class="clear_float"></div>
    <p class="quicksand">For an appointment, contact Johan de Bruyn on 021 976 3180 or 082 881 2011 or <a href="mailto:johan@louwcoet.co.za">johan@louwcoet.co.za</a></p>
</div>
  
<div class="main-container-contact-wide">
    <figure class="wide-logo-contact">
        <img src="images/caf-logo.jpg" alt="" id="logo-contact"/>
    </figure>   

    <div class="main_heading contact-heading">
        <h1 class="title title-mid contact-title">PRICE LIST</h1>
        
    </div>
    
    <div class="clear_float"></div>
    
</div>

<section class="section-middle">
    <div class="download">
        <a href="uploaded-pdfs/<?php echo $rs_docs_rows['ppdf']; ?>" target="_blank" class="download-link">DOWNLOAD PRICE LIST HERE</a>
    </div>
    
    <div class="download">
        <a href="uploaded-pdfs/<?php echo $rs_docs_rows['ppdf']; ?>" target="_blank" class="download-link">DOWNLOAD FULL SITE PLAN HERE</a>
    </div>
    <div class="clear_float"></div>
</section>

<section class="section-bottom">

	<div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?>
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('PRICE')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('PRICE')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Price')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Price')").addClass("current-menu-item-a");
    
    var container_width = $(".main-container-contact-wide").width(); 
    var container_height = $(".main-container-contact-wide").height(); 
    
    var percentageoff = (3/100 * container_width)*2;
    
    var new_height = container_height - percentageoff;
    
    $(".contact-heading").css("height", new_height);
    
    $(".contact-title").css("line-height", new_height + 'px');
});

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
