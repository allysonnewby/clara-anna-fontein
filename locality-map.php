<!doctype html>
<html>
<head>
<?php require('head-content.php'); ?>
<title>Locality Map | Clara Anna Fontein</title>
</head>

<body>
<?php require('menu-desktop.php');?>
<?php require('mobile-nav.php'); ?>    
<a href="#"><img src="images/ladybug-cropped.png" style="float: right;" id="ladybug"></a>
<div id="wrapper">



<div class="main_heading">
	<h1 class="title title-top">TO COMPREHEND IT YOU HAVE TO SEE IT. SAMPLE IT. SAVOUR IT...</h1>
    <h4 class="cinzel jules">- ANONYMOUS</h4>
</div>

<figure class="mobile-logo">
	<img src="images/caf-logo-mobile.jpg" alt="" id="logo-mobile"/>
</figure>

<div class="main-container">
    <figure class="wide-logo">
        <img src="images/caf-logo.jpg" alt="" id="logo"/>
    </figure>
    <figure class="main-pic-1">
        <img src="images/locality-1.jpg"/>
    </figure>
    <figure class="main-pic-2">
        <img src="images/locality-2.jpg" alt="" id="logo"/>
    </figure>
    
    <div id="slidy-container">
	<p id="banner">SECURE ESTATE LIVING - DURBANVILLE</p>
        <figure id="slidy">
            <img src="images/cgi4.jpg" alt=""/>
            <img src="images/cgi1.jpg"  alt=""/>
        </figure>
        <img src="images/short-red-flash-home.png" id="red-banner" alt="Phase 2 on show now">
        <?php require('butterfly.php');?>
    </div>

    
    <div class="clear_float"></div>
</div>

<p class="bilbo locality-mid-p">There’s truly never a dull moment at Clara Anna Fontein. It’s a place where neither mind nor body are left idle, and with the large number of activities available on-site, a well-balanced life is easily achieved. And, should you desire to venture into the Durbanville area, you won’t have to wander too far to find exactly what you’re looking for.</p>

<section class="section-middle">
	<figure class="fig zoomed-out">
    	<img src="images/locality-map.jpg" alt=""/ class="map-fig">
    </figure>
</section>

<section class="section-bottom">
    <h2 class="open-san directions-heading">DIRECTIONS</h2>
	<article class="locality-article">	
    	<p class="quicksand">From Cape Town: follow the N1 towards Paarl. Take exit 23 off the N1. Turn left onto Willie Van Schoor Ave. Continue onto Durbanville Ave. Continue onto Main St. At the 1st circle, take 2nd exit onto Vissershok Rd (M48). At the 2nd circle, take 2nd exit onto Vissershok Rd. Clara Anna Fontein is along Vissershok Rd, on the left hand side.</p>
    </article>
    
    <article class="locality-article">
        <p class="quicksand">From N7: take the N1 towards Goodwood/Malmesbury.  Keep left at the fork to exit 13A and follow the signs for N7/Malmesbury and merge onto the N7.  Turn right onto Vissershok Rd (M48).  Turn right, Destination will be on the right.</p>
    </article>
    
    <div class="clear_float"></div>
</section>

</div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php require('scripts.php'); ?> 
<script>
$(document).ready(function(){
	
	$( "#desktop_menu li a:contains('LOCALITY')").parent(this).addClass("current-menu-item");
	$( "#desktop_menu li a:contains('LOCALITY')").addClass("current-menu-item-a");
    
    $( ".mobile_nav li a:contains('Locality')").parent(this).addClass("current-menu-item");
	$( ".mobile_nav li a:contains('Locality')").addClass("current-menu-item-a");
	// run test on initial page load
    checkSize();

    // run test on resize of the window
    $(window).resize(checkSize);	
	
	//Function to the css rule
	function checkSize(){
	if ($("#desktop_menu").css("display") == "block" ){
	
	$('.map-fig').mouseenter(function(){	
		if($('.section-middle figure').hasClass("zoomed-out")){
			$(this).css("cursor", "zoom-in");
			}else if($('.section-middle figure').hasClass("zoomed-in")){
				$(this).css("cursor", "zoom-out");
				}
		});
	
	$('.section-middle figure').click(function(){
		
		if($('.section-middle figure').hasClass("zoomed-out")){
			$('.map-fig').attr("src","images/locality-map-2.jpg");
			$('.map-fig').css("transform", "scale(2)");
			$('.map-fig').css("margin-top", "0%");
			$(this).removeClass("zoomed-out");
			$(this).addClass("zoomed-in");
			
			if($('.section-middle figure').hasClass("zoomed-in")){
				  $('.map-fig').css("cursor", "zoom-out");
				  }
			
			}else{
				$('.map-fig').css("transform", "scale(1)");
				$('.map-fig').css("margin-top", "2%");
				$('.map-fig').attr("src","images/locality-map.jpg");
				$(this).addClass("zoomed-out");
				$(this).removeClass("zoomed-in");
				if($('.section-middle figure').hasClass("zoomed-out")){
					$('.map-fig').css("cursor", "zoom-in");
					}
				}
		});
		
		
	}
}

});

</script>
</body>
<?php require('detect-ie.php');?>    
</html>
